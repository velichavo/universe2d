﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Rendering.Universal;
using U2D;

namespace  T2D
{
	[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
	public class Terrain2D : MonoBehaviour, IEntityCollision
	{
		[SerializeField] private SurfaceTypeMaterial _surfaceType;

		private MeshFilter meshFilter;
		private ShadowCaster2D shadowCaster;
		[SerializeField] private List<Vector3> nodes = new();
		private Mesh mesh2D;
		public Mesh Mesh2D 
		{
			get => mesh2D; 
			set => mesh2D = value;
		}

		public ShadowCaster2D ShadowCaster2DComponent
		{
			get => shadowCaster;
			set => shadowCaster = value;
		}
        public SurfaceTypeMaterial SurfaceType { get => _surfaceType; }
        public GameObject CollisionGameobject { get => gameObject;}

        private void OnEnable()
		{
			
			if (nodes.Count == 0)
			{
				TerrainReset();
			}
		}

		public Mesh TerrainReset()
		{
			mesh2D = new Mesh();
			meshFilter = GetComponent<MeshFilter>();
			meshFilter.sharedMesh = mesh2D;
			shadowCaster = GetComponent<ShadowCaster2D>();
			return mesh2D;
		}

		public void NodesClear()
		{
			nodes.Clear();
			nodes.Add(new Vector3(-1, 0, 0));
			nodes.Add(new Vector3(-1, 1, 0));
			nodes.Add(new Vector3(1, 1, 0));
			nodes.Add(new Vector3(1, 0, 0));
		}

		public Vector3[] GetNodes()
		{
			return nodes.ToArray();
		}

		public void InsertNode(int nodeIndex, Vector3 position)
		{
			nodes.Insert(nodeIndex, position);
		}

		public void RemoveNode(int indexToDelete)
		{
			nodes.RemoveAt(indexToDelete);
		}
		public void ChangeNode(int nodeIndex, Vector3 newPosition)
		{
			nodes[nodeIndex] = newPosition;
		}

        public void OnBulletCollision(float damage)
        {
            //Debug.Log($"Столкновение со стеной");
        }
    }
}
