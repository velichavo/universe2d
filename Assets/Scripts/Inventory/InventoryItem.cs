using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public class InventoryItem
    {
        private bool _isEquip;
        private int _amount;
        private ICommonItem _item;

         public bool IsEquip => _isEquip;
        
        public InventoryItem(ICommonItem item, int count)
        {
            Add(item, count);
        }

        public void Add(ICommonItem item, int count)
        {
            if(_item == null)
            {
                _item = item;
                _amount = count;
            }
            else
            {
                _amount += count;
                item.ItemDestroy();
            }
        }

        public bool Remove(ICommonItem item, int amount, out int remain)
        {
            if(amount >= _amount)
            {
                item.ItemDestroy();
                remain = _amount;
                return true;
            }
            else
            {
                _amount -= amount;
                remain = amount;
                return false;;
            }
        }

        public int GetAmount()
        {
            return _amount;
        }
        
        public ICommonItem GetItem()
        {
            return _item;
        }
        public bool GetEquip()
        {
            return _isEquip;
        }

        public void Equip(bool isEquip)
        {
            //Возвращает истина если оружие совпадает с тем что экипировано и выходит, иначе текущее разэкипируется и ложь. Далее екмпируем новый предмет
            if (_item.Owner.ActionItem.Equip(this, isEquip)) return;
            _isEquip = isEquip;
            _item.Equip(_isEquip);
            _item.Owner.ActionItem.TryTake();//Устанавливаем положение премета в руках или за спиной
        }

        internal void Remove(ICommonItem item, int amount)
        {
            throw new NotImplementedException();
        }
    }
}