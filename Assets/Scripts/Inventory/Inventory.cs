using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace U2D
{
    public class Inventory
    {
        private List<InventoryItem> _inventoryItems = new List<InventoryItem>();

        public void Add(ICommonItem item, int count)
        {
            InventoryItem inventoryItem = GetInventoryItemByType(item.ItemConfig.ItemType);
            if(inventoryItem == null || item.ItemConfig.EachInNewSlot)
            {
                _inventoryItems.Add(new(item, count));
            }
            else
            {
                inventoryItem.Add(item, count);
            }
        }

        public bool Remove(ICommonItem item, int amount, out int remain)
        {
            InventoryItem inventoryItem = GetInventoryItemByType(item.ItemConfig.ItemType);
            if (inventoryItem == null)
            {
                remain = 0;
                return false;
            }

            if (inventoryItem.Remove(item, amount, out remain))
            {
                _inventoryItems.Remove(inventoryItem);
                return true;
            }
            return false;
        }

        public List<InventoryItem> GetAllItems()
        {
            return _inventoryItems.ToList();
        }

        public InventoryItem GetInventoryItem(string itemType)
        {
            InventoryItem item = GetInventoryItemByType(itemType);
            return item == null ? null : item;
        }

        private InventoryItem GetInventoryItemByType(string itemtype)
        {
            return _inventoryItems.Find(i => i.GetItem().ItemConfig.ItemType == itemtype);
        }
    }
}