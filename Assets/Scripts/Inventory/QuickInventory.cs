using System;

namespace U2D
{
    public class QuickInventory
    {
        public const int MaxItemInQiuckSlots = 12;
        private InventoryItem[] _qickSlots;
        public SwitchQuickSlot SwitchQuickSlot{get; private set;} 

        public QuickInventory(CharacterActionItem actionItem)
        {
            SwitchQuickSlot = new(actionItem);
            _qickSlots = new InventoryItem[MaxItemInQiuckSlots];
        }

        public InventoryItem GetInventoryItem(int index)
        {
            return _qickSlots[index];
        }

        public void SetQiuckSlot(InventoryItem inventoryItem, int index)
        {
            _qickSlots[index] = inventoryItem;
        }

        internal InventoryItem[] GetAllItems()
        {
            return _qickSlots;
        }
    }
}