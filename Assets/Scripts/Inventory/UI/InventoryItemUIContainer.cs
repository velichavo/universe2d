using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace U2D
{
    public class InventoryItemUIContainer : MonoBehaviour
    {
        [SerializeField] InventoryItemUI original;
        [SerializeField] Transform parent;
        [SerializeField] private List<InventoryItemUI> _inventoryItemUIs;

        public void Init()
        {
            _inventoryItemUIs = parent.GetComponentsInChildren<InventoryItemUI>(true).ToList();
            foreach(InventoryItemUI item in _inventoryItemUIs)
            {
                item.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            foreach(InventoryItemUI item in _inventoryItemUIs)
            {
                item.gameObject.SetActive(item.IsActive);
            }
        }

        public void UpdateContainer(List<InventoryItem> items)
        {
            foreach (var itemUI in _inventoryItemUIs)
            {
                itemUI.Remove();
            }

            for(int i = 0; i < _inventoryItemUIs.Count; i++)
            {
                if(i < items.Count)
                {
                    _inventoryItemUIs[i].Set(items[i]);
                }
            }
            
            for(int i = _inventoryItemUIs.Count; i < items.Count; i++)
            {
                InventoryItemUI itemUI = Instantiate(original, parent.transform);
                itemUI.Set(items[i]);
                itemUI.OnEnable();
                _inventoryItemUIs.Add(itemUI);
            }
        }

        public void ResetInventoryItemUI()
        {
            foreach (var itemUI in _inventoryItemUIs)
            {
                itemUI.Reset();
            }
        }
    }
}