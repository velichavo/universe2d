using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace U2D
{
    public class InventoryItemUI: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] TextMeshProUGUI _itemUIText;
        [SerializeField] TextMeshProUGUI _itemUIAmount;
        [SerializeField] Toggle _isEquipUIToggle;

        private Color _defaultColor;
        private InventoryItem _inventoryItem;
        private InventoryUI _inventoryUI;

        public  bool IsActive {get; private set;}

        private void Start()
        {
            _isEquipUIToggle.onValueChanged.AddListener(ToggleValueChanged);
            _inventoryUI = GetComponentInParent<InventoryUI>(true);
        }

        public void Set(InventoryItem inventoryItem)
        {
            _inventoryItem = inventoryItem;
            IsActive = true;
            gameObject.SetActive(true);
        }

        public void Remove()
        {
            _inventoryItem = null;
            IsActive = false;
            gameObject.SetActive(false);
        }

        public void OnEnable()
        {
            if (_inventoryItem == null) return;
            _itemUIText.text = _inventoryItem.GetItem().ItemConfig.Title;
            _itemUIAmount.text = _inventoryItem.GetAmount().ToString();
            _isEquipUIToggle.isOn = _inventoryItem.GetEquip();
            if(_defaultColor == Color.clear)
                _defaultColor = _itemUIText.color;
        }

        private void ToggleValueChanged(bool change)
        {
            _inventoryItem.Equip(change);
        }

        private void OnDestroy()
        {
            _isEquipUIToggle.onValueChanged.RemoveListener(ToggleValueChanged);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _inventoryUI.SetPointerEnterInventoryItem(_inventoryItem);
            _itemUIText.color = Color.red;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Reset();
        }

        public void Reset()
        {
            if(_inventoryUI == null) return;

            if(_inventoryUI.ObservedInventoryUIState.Value != InventoryUIState.SetQuickSlot)
            {
                _inventoryUI.SetPointerEnterInventoryItem(null);
                _itemUIText.color = _defaultColor;
            }
        }
    }
}