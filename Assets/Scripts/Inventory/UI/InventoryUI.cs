//Подписываюсь на GlobalGameState. В игроке меняем GlobalGameState на Inventory. Вызывает метод OnChangedGameState включаем инвентарь. Меняем в ShowInventory на Play.  Вызывает метод OnChangedGameState выключаем инвентарь

using System;
using System.Collections.Generic;
using NTC.Singleton;
using UnityEngine;

namespace U2D
{
    public enum InventoryUIState {Default, SetQuickSlot}

    public class InventoryUI : MonoBehaviour
    {
        public Func<CharacterActionItem> GetActionItem;
        [SerializeField] private InventoryItemUIContainer _playerInventoryItemUIContainer;
        [SerializeField] private InventoryItemUIContainer _otherInventoryItemUIContainer;
        [SerializeField] private RectTransform _dimmer;
        private WorldCore _world;
        private HandlingInput _handlingInputUI;
        private HandlingInput _handlingInputUIOuickInventory;
        private QuickInventoryUI _quickInventoryUI;
        private InventoryItem _pointerEnterInventoryItem;

        public ObservedValue<InventoryUIState> ObservedInventoryUIState {get; set;} = new();
        

        public void Init(Func<CharacterActionItem> getActionItem)
        {
            _world = Singleton<WorldCore>.Instance;
            _world.GlobalGameState.OnChanged += OnChangedGameState;

            GetActionItem = getActionItem;
            _playerInventoryItemUIContainer.Init();
            _quickInventoryUI = GetComponentInChildren<QuickInventoryUI>();
            _quickInventoryUI.Init(this);

            InitInputUI();
            InitInputUIQuickInventory();
        }

        private void InitInputUIQuickInventory()
        {
            _handlingInputUIOuickInventory = new();
            List<Command> commands = new()
            {
                new HandleInventoryUICommand(ReturnInInventory, KeyCode.Escape)
            };
            _handlingInputUIOuickInventory.Init(commands);
        }

        private void InitInputUI()
        {
            _handlingInputUI = new();
            List<Command> commands = new()
            {
                new ShowInventoryCommand(HideInventory, KeyCode.I),
                new HandleInventoryUICommand(SetItemQuickInventory, KeyCode.B),
                new HandleInventoryUICommand(HideInventory, KeyCode.Escape)
            };
            _handlingInputUI.Init(commands);
        }

        private void HideInventory()
        {
            _world.GlobalGameState.Value = GameState.Play;
            _dimmer.gameObject.SetActive(false);
        }

        private void SetItemQuickInventory()
        {
            if(_pointerEnterInventoryItem != null)
            {
                ObservedInventoryUIState.Value = InventoryUIState.SetQuickSlot;
                _dimmer.gameObject.SetActive(true);
            }
        }

        private void ReturnInInventory()
        {
             _dimmer.gameObject.SetActive(false);
            ObservedInventoryUIState.Value = InventoryUIState.Default;
        }

        public void OnChangedGameState(object o)
        {
            if(_world.GlobalGameState.Value == GameState.InventoryMenu)
            {
                gameObject.SetActive(true);
                _playerInventoryItemUIContainer.UpdateContainer(GetActionItem.Invoke().ItemInventory.GetAllItems());
                _quickInventoryUI.UpdateItem(GetActionItem.Invoke().Quick.GetAllItems());
                ReturnInInventory();
                _playerInventoryItemUIContainer.ResetInventoryItemUI();
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        public void CustomUpdate()
        {
            switch (ObservedInventoryUIState.Value)
            {
                case InventoryUIState.Default:
                    _handlingInputUI.CustomUpdate();
                    break;
                case InventoryUIState.SetQuickSlot:
                    _handlingInputUIOuickInventory.CustomUpdate();
                    break;
            }
        }

        public void SetPointerEnterInventoryItem(InventoryItem inventoryItem)
        {
            _pointerEnterInventoryItem = inventoryItem;
        }
        private void OnDestroy()
        {
            _world.GlobalGameState.OnChanged -= OnChangedGameState;
        }

        internal void SetQuickslot(int index)
        {
            if(_pointerEnterInventoryItem != null)
            {
                GetActionItem.Invoke().Quick.SetQiuckSlot(_pointerEnterInventoryItem, index);//Обновляем быстрый слот
                _quickInventoryUI.UpdateItem(GetActionItem.Invoke().Quick.GetAllItems());//Обновляем отображение быстрых слотов
                ReturnInInventory();//выходим из режима установки предмета в быстрый слот
                _playerInventoryItemUIContainer.ResetInventoryItemUI();//сбрасываем подсветку предмета в инвентаре
            }
        }
    }
}