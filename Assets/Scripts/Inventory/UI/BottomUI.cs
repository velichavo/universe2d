using UnityEngine;
using TMPro;
using System;

namespace U2D
{
    public enum HUDTextType {Health, Clip, Ammo}
    public class HUD : MonoBehaviour
    {
        public Func<PlayerActionItem> GetCommonAction;

        [SerializeField] private QuickInventoryImageFitter _weaponIcon;
        [SerializeField] private QuickInventoryImageFitter _ammoIcon;
        [SerializeField] private TextMeshProUGUI _healthText;
        [SerializeField] private TextMeshProUGUI _clipAmountText;
        [SerializeField] private TextMeshProUGUI _ammoAmounthText;

        private PlayerActionItem characterActionItem;

        public void Init(PlayerActionItem actionItem)
        {
            characterActionItem = actionItem;
            characterActionItem.OnChangedUIIcon += SetIcon;
            characterActionItem.OnChangedHUDText += SetHUDText;
            _weaponIcon.Init();
            _ammoIcon.Init();
        }

        /// <summary>
        /// Устанавливает в HUD спрайты оружия и паронов, если передать null, то скрывает спрайты.
        /// </summary>
        public void SetIcon(Sprite spriteWeapon, Sprite spriteAmmo)
        {
            _weaponIcon.SetImageInSlot(spriteWeapon);
            _ammoIcon.SetImageInSlot(spriteAmmo);
        }
        public void SetHUDText(HUDTextType type, int amount)
        {
            switch (type)
            {
                case HUDTextType.Health:
                    _healthText.text = amount.ToString();
                    break;
                case HUDTextType.Clip:
                    _clipAmountText.text = amount.ToString();
                    break;
                case HUDTextType.Ammo:
                    _ammoAmounthText.text = amount.ToString();
                    break;
            }
        }

        private void OnDestroy()
        {
            characterActionItem.OnChangedUIIcon -= SetIcon;
            characterActionItem.OnChangedHUDText -= SetHUDText;
        }
    }
}