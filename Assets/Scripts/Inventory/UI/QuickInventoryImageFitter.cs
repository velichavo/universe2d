using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace U2D
{
    public class QuickInventoryImageFitter : MonoBehaviour, IPointerClickHandler
    {
        public event Action<QuickInventoryImageFitter> OnClickQuickSlot;
        [SerializeField] bool _fitY;
        [SerializeField] private float _boundImage;
        [SerializeField] private float _ratio = 0.5f;
        [SerializeField] private Sprite _testSprite;

        private Image _image;
        private RectTransform _rectTransformImage;
        private RectTransform _rectTransform;

        public Sprite TestSprite => _testSprite;

        public void Init()
        {
            _rectTransform = GetComponent<RectTransform>();
            _rectTransformImage = GetComponentsInChildren<RectTransform>(true).Last();
            _image = GetComponentsInChildren<Image>(true).Last();
        }

        public void SetImageInSlot(Sprite sprite)
        {
            if(sprite == null)
            {
                RemoveSprite();
                return;
            }

            _image.sprite = sprite;
            _rectTransformImage.gameObject.SetActive(true);
            Vector2 targetSprteSize = sprite.bounds.size * sprite.pixelsPerUnit * _ratio;
            float r = 1.0f;
            if(_fitY)
            {
                r = (_rectTransform.sizeDelta.y - _boundImage) / targetSprteSize.y;
            }
            else
            {
                r = (_rectTransform.sizeDelta.x - _boundImage) / targetSprteSize.x;
            }
            _rectTransformImage.sizeDelta = new Vector2(targetSprteSize.x * r, targetSprteSize.y * r);
        }

        public void RemoveSprite()
        {
            _rectTransformImage.gameObject.SetActive(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {   
            OnClickQuickSlot?.Invoke(this);
        }
    }
}
