using System;
using JetBrains.Annotations;
using UnityEngine;


namespace U2D
{
    public class QuickInventoryUI : MonoBehaviour
    {
        [SerializeField] private QuickInventoryImageFitter _prefab;
        [SerializeField] private int _maxItems = 12;

        [SerializeField] private QuickInventoryImageFitter[] _quickItems;

        private InventoryUI _inventoryUI;

        public void Init(InventoryUI inventoryUI)
        {
            _inventoryUI = inventoryUI;
            _quickItems = new QuickInventoryImageFitter[_maxItems];
            QuickInventoryImageFitter[] quickItems = GetComponentsInChildren<QuickInventoryImageFitter>();
            for(int i = 0; i < _maxItems; i ++)
            {
                QuickInventoryImageFitter item = null;

                if(i < quickItems.Length)
                {
                    item = quickItems[i];
                }

                if(item == null)
                {
                    item = Instantiate(_prefab, transform);
                }
                _quickItems[i] = item;

                item.transform.name = $"{_prefab.name}{i}";
                item.Init();
                item.OnClickQuickSlot += OnClickQuickSlot;
            }
        }

        public void UpdateItem(InventoryItem[] items)
        {
            for(int i = 0; i < _quickItems.Length; i++)
            {
                InventoryItem item = items[i];
                if (item != null)
                {
                    _quickItems[i].SetImageInSlot(item.GetItem().ItemConfig.SpriteItem);
                }
                else
                {
                    _quickItems[i].RemoveSprite();
                }
            }
        }

        public void OnClickQuickSlot(QuickInventoryImageFitter item)
        {
            switch (_inventoryUI.ObservedInventoryUIState.Value)
            {
                case InventoryUIState.Default:
                    _inventoryUI.GetActionItem.Invoke().QuickEquip(Array.IndexOf(_quickItems, item));
                    break;
                case InventoryUIState.SetQuickSlot:
                    _inventoryUI.SetQuickslot(Array.IndexOf(_quickItems, item));
                    break;
            }
        }

        private void OnDestroy()
        {
            foreach(QuickInventoryImageFitter item in _quickItems)
            {
                item.OnClickQuickSlot -= OnClickQuickSlot;
            }
        }
    }
}