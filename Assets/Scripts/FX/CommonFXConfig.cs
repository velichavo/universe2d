using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "FXConfig", menuName = "Gameplay/FXConfig")]
    public class CommonFXConfig : ScriptableObject
    {
        [SerializeField] private GameObject _fXPrefab;

        public GameObject FXPrefab => _fXPrefab;
    }
}