using UnityEngine;

namespace U2D
{
    public enum CommonFXType {none, Body, SoftBody, Metal, Concrete, Sand, Dirt, Wood, SmallExplosion}
    public class CommonFX : MonoBehaviour
    {
        [SerializeField] private CommonFXConfig _config;
        private ParticleSystem _fx;
        private FXPool _pool;

        public CommonFX Init(FXPool pool) 
        {
            _fx = GetComponent<ParticleSystem>();
            _pool = pool;
            return this;
        }

        public void FXPlay(Vector3 position, Quaternion rotation)
        {
            transform.position = position;
            transform.rotation = rotation;
            _fx.Play(true);
        }

        public void OnParticleSystemStopped()
        {
            _pool.Pool.Return(this);
        }
    }
}