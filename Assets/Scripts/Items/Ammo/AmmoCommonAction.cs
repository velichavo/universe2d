using System;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class AmmoCommonAction : ICommonAction
    {
        [SerializeField] private int _currentClipCapacity;
        private CommonAmmo _ammo;

        public int CurrentClipCapacity
        {
            get => _currentClipCapacity;
            set => _currentClipCapacity = value;
        }
        public AmmoConfig[] AmmoTypeConfigs {get; private set;}
        public int CurrenAmmoType {get; set;}

        public ItemAnimator CommonAnimator {get; private set;} = new();

        public void Init(CommonAmmo ammo, AmmoConfig[] ammoConfigs, int ammoType)
        {
            AmmoTypeConfigs = ammoConfigs;
            _ammo = ammo;
            CurrenAmmoType = ammoType;
        }
        public void Arming(bool isArming)
        {
            
        }
    }
}