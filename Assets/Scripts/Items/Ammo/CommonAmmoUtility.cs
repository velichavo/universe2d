using UnityEngine;

namespace U2D
{
    public class CommonAmmoUtility : CommonItemUtility
    {
        private CommonAmmo _ammo;

        public CommonAmmoUtility(CommonAmmo ammo) : base(ammo)
        {
            _ammo = ammo;
        }
    }
}