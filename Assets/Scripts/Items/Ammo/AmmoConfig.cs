using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "BulletInfo", menuName = "Gameplay/Ammo")]
    public class AmmoConfig : CommonItemConfig
    {
        [SerializeField] private Sprite _sprite;
        [SerializeField] private Sprite _spriteLight;
        [SerializeField] private float _timeTrailMove;
        [SerializeField] private float _timeTrailCollision;
        [SerializeField, Range(0, 1)] private float _bounceSlopeHeight;
        [SerializeField, Range(0, 1)] private float _bounceChance;
        [SerializeField] float _bounceCount;
        [SerializeField] private BulletType _type;
        [SerializeField] private LayerMask _collisonMask;

        public Sprite BulletSprite => _sprite;
        public Sprite BulletSpriteLight => _spriteLight;
        public float TimeTrailMove => _timeTrailMove;
        public float TimeTrailCollision => _timeTrailCollision;
        public LayerMask CollisionMask => _collisonMask;
        public float _BounceSlopeHeight => _BounceSlopeHeight;
        public BulletType CommonBulletType => _type;
        public float BounceCount => _bounceCount;
        public float BounceChance => _bounceChance;
    }
}