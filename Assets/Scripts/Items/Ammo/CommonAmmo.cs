using UnityEngine;

namespace U2D
{
    public class CommonAmmo : CommonItem
    {
        [SerializeField] private AmmoConfig _config;
        [SerializeField] private int _amount;
        [SerializeField] private AmmoCommonAction _commonAction;

        private CommonAmmoUtility _utility;

        public override CommonItemConfig ItemConfig => _config;
        public AmmoConfig Config => _config;
        public override CommonItemUtility Utility => _utility;

        public override int Amount => _amount;

        public override ICommonAction ItemCommonAction => _commonAction;

        public override void Init()
        {
            if(_amount <= 0) _amount = _config.Amount;
            _utility = new(this);
            _commonAction = new();
            _commonAction.Init(this, _config.AmmoTypeConfig, _config.DefaultAmmoType);
        }
    }
}