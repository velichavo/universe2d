using UnityEngine;

namespace U2D
{
    public class Weapon : CommonItem, IWeapon
    {
        [SerializeField] private WeaponConfig _config;
        
        [SerializeField, Tooltip("Конфиг будет меняться при перезарядке другими патронами из инветаря")] private AmmoConfig _ammoTypeConfig;
        [SerializeField] private WeaponCommonAction _commonAction;

        private WeaponUtility _utility;

        public override CommonItemConfig ItemConfig => _config;
        public WeaponConfig Config => _config;
        public override CommonItemUtility Utility => _utility;
        public override ICommonAction ItemCommonAction => _commonAction;
        public override int Amount => _config.Amount;

        public override void Init()
        {
            _utility = new(this);
            _commonAction.Init(this, _config.AmmoTypeConfig, _config.DefaultAmmoType);
        }

        public override void Equip(bool isEquipped)
        {
            base.Equip(isEquipped);
            if(isEquipped)
            {
                Owner.Rig.RightLimbSolver2D.GetChain(0).target = _utility.GripTransform;
                Owner.Rig.LeftLimbSolver2D.GetChain(0).target = _utility.HandGuardTransform;
            }
            else
            {
                Owner.Rig.RightLimbSolver2D.GetChain(0).target = null;
                Owner.Rig.LeftLimbSolver2D.GetChain(0).target = null;
            }
        }

        public void Reload(int amount)
        {
            _commonAction.Attack.ReloadWeapon(amount);
        }
        public override void Action(GetKeyState state)
        {
            _commonAction.Attack.Attack(state, _ammoTypeConfig);
        }

        /* void OnDrawGizmos( )
        {
            if(_character && _isEuqip)
                Handles.Label(_character.transform.position - Vector3.left, $"direction: {(CameraTracker.WorldMousePos - transform.position).magnitude}");
        } */
    }
}