using System;
using System.Collections;
using UnityEngine;

public class CommonClip : MonoBehaviour
{
    public Action<CommonClip> OnEndTimeLife;
    [SerializeField] float _timeLife;
    [SerializeField] float _force;

    private Rigidbody2D _rB2D;

    public void ClipStart(Transform transform)
    {
        this.transform.SetPositionAndRotation(transform.position, transform.rotation);
        _rB2D.AddForce(_force * -transform.up, ForceMode2D.Impulse);
        StartCoroutine(EndTimeLife());
    }

    public void Init(Action<CommonClip> action)
    {
        OnEndTimeLife = action;
        _rB2D = GetComponent<Rigidbody2D>();
    }

    private IEnumerator EndTimeLife()
    {
        yield return new WaitForSeconds(_timeLife);
        OnEndTimeLife.Invoke(this);
    }
}
