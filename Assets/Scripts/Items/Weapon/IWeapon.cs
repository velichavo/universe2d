namespace U2D
{
    public interface IWeapon
    {
        void Reload(int amount);
    }
}