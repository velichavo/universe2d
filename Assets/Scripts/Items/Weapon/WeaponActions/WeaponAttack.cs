using System.Collections;
using UnityEngine;

namespace U2D
{
    public enum ShootState {None = 0, Ready, StartShoots, Shoots, TimeoutShoot, EndShoots, Reload, NoAmmo}
    public class WeaponAttack
    {
        private Weapon _weapon;
        private ShootState _shootState;
        private GetKeyState _keyState;
        private WeaponConfig _config;
        private WeaponCommonAction _commonAction;
        private WeaponUtility _weaponUtility;

        public WeaponAttack(Weapon weapon, WeaponCommonAction commonAction)
        {
            _weapon = weapon;
            _commonAction = commonAction;
            _config = _weapon.Config;
            _weaponUtility = (WeaponUtility)_weapon.Utility;
            _shootState = ShootState.Ready;
        }

        public void Attack(GetKeyState state, AmmoConfig bulletConfig)
        {
            _keyState = state;
            switch (_shootState)
            {
                case ShootState.Ready:
                    if(_commonAction.CurrentClipCapacity == 0) 
                    {
                        _weapon.Owner.ActionItem.Reload();
                    }
                    else
                    {
                        if(_config.StartShootTime > 0)
                            _weapon.StartCoroutine(StartShoot());
                        else 
                            _shootState = ShootState.Shoots;
                    }
                    break;
                case ShootState.Shoots:
                    for(var i = 0; i < _config.BulletAmountPerShot; i++)
                    {
                        CommonBullet newBullet = _weapon.World.Pools.GlobalPoolBullet.Pool.Get();
                        _weapon.StartCoroutine(_weaponUtility.SpriteShotFlash.Flash(_config.FlashTime));
                        newBullet.BulletStart(bulletConfig, _weapon, (bullet) => _weapon.World.Pools.GlobalPoolBullet.Pool.Return(newBullet));//Передать ссылку на метод в пулю и вместо дестрой вызвать этот метод
                        CommonClip newClip = _weapon.World.Pools.GlobalPoolClip.Pool.Get();
                        newClip.ClipStart(_weaponUtility.EjectorTransform);
                    }
                    _weapon.StartCoroutine(TimeoutShoot());
                    EndShoot();
                    break;
                case ShootState.TimeoutShoot:
                    EndShoot();
                    break;
                case ShootState.NoAmmo:
                    if (state == GetKeyState.GetKeyDown)
                    {
                        NoAmmo();
                    }
                    break;
                default:
                    break;
            }
        }

        public void ReloadWeapon(int count)
        {
            if(_shootState != ShootState.Reload)
                _weapon.StartCoroutine(Reload(count));
        }

        public void UnloadWeapon()
        {

        }

        private IEnumerator TimeoutShoot()
        {
            _commonAction.CurrentClipCapacity -= 1;
            _weapon.Owner.ActionItem.ClipStateChangedHUD(_weapon.ItemCommonAction.CurrentClipCapacity);
            _shootState = ShootState.TimeoutShoot;
            _commonAction.ActionWeaponRotation.PlayRecoil(_config.ShootTime);
            _weaponUtility.Audio.PlayOneShot(_config.ShootSound);
            yield return new WaitForSeconds(_config.ShootTime);
            if (_keyState == GetKeyState.GetKeyUp)
                _shootState = ShootState.Ready;
            else
                _shootState = ShootState.Shoots;
            if(_commonAction.CurrentClipCapacity == 0)
            {
                EndShootSoundPlay();
                Debug.Log(_shootState);
                _weapon.Owner.ActionItem.Reload();
            }
        }
        private IEnumerator Reload(int count)//подаем уже заранее недостающее количество патронов
        {
            if(count > 0)
            {
                _shootState = ShootState.Reload;
                float rt = _config.ReloadTime;
                int cp = 1;
                if (_config.IsPlayReloadSoundEqualClipCapacity)
                {
                    rt = _config.ReloadTime / _config.ClipCapacity * count;
                    cp = count ;
                }
                
                for(int i = 1; i <= cp; i++)
                {
                    _weaponUtility.Audio.PlayOneShot(_config.ReloadSound);
                    yield return new WaitForSeconds(rt / cp);
                    _commonAction.CurrentClipCapacity += i * count;
                }
                _weapon.Owner.ActionItem.ClipStateChangedHUD(_weapon.ItemCommonAction.CurrentClipCapacity);
                _weapon.Owner.ActionItem.AmmoStateChangedHUD(_weapon.ItemCommonAction.AmmoTypeConfigs[_weapon.ItemCommonAction.CurrenAmmoType].ItemType);
                _shootState = ShootState.Ready;
            }
            else
            {
                _weapon.Owner.ActionItem.ClipStateChangedHUD(_weapon.ItemCommonAction.CurrentClipCapacity);
                _weapon.Owner.ActionItem.AmmoStateChangedHUD(_weapon.Owner.ActionItem.GetAmmoType(_weapon));
                if(_weapon.ItemCommonAction.CurrentClipCapacity > 0)
                {
                    _shootState = ShootState.Ready;
                }
                else
                {
                    NoAmmo();
                }
            }

        }
        private IEnumerator StartShoot()
        {
            _shootState = ShootState.StartShoots;
            _weaponUtility.Audio.clip = _config.StartShootSound;
            _weaponUtility.Audio.Play();
            yield return new WaitForSeconds(_config.StartShootTime);
            if (_keyState == GetKeyState.GetKeyUp)
                _shootState = ShootState.Ready;
            else
                _shootState = ShootState.Shoots;
        }
        private void EndShoot()
        {
            if (_keyState == GetKeyState.GetKeyUp)
            {
                if (_config.EndShootTime > 0) 
                    EndShootSoundPlay();
            }
        }
        private void EndShootSoundPlay()
        {
            if (_config.EndShootSound)
            {
                _weaponUtility.Audio.clip = _config.EndShootSound;
                _weaponUtility.Audio.Play();
            }
        }

        private void NoAmmo()
        {
            _weaponUtility.Audio.PlayOneShot(_config.NoAmmoSound);
            _shootState = ShootState.NoAmmo;
        }
    }
}