using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public class WeaponRotation : IActionUpdate
    {
        public const float AnimationCurveTime = 1.0f;

        private float _timeRecoil;
        private float _deltaTime;
        private Quaternion RotationBackpack = Quaternion.Euler(0, 0, 90.0f);
        private Weapon _weapon;
        private WeaponUtility _weaponUtility;
        private WeaponConfig _config;
        private ItemAnimator _weaponAnimator;

        public WeaponRotation(Weapon weapon)
        {
            _weapon = weapon;
            _weaponUtility = (WeaponUtility)_weapon.Utility;
            _config = (WeaponConfig)_weapon.ItemConfig;
            _weaponAnimator = _weapon.ItemCommonAction.CommonAnimator;
        }

        public void CustomUpdate()
        {
            if(!_weapon.Owner) return;

            _weapon.transform.localScale = new(Mathf.Abs(_weapon.transform.localScale.x), _weapon.Owner.CommonAction.ActionCharacterRotation.FlipBody * Mathf.Abs(_weapon.transform.localScale.y));

            if(!_weapon.IsTaken)
            {
                _weapon.transform.position = _weapon.Owner.Rig.Backpack.transform.position;
                _weapon.transform.rotation = _weapon.Owner.Rig.Backpack.transform.rotation;                
                _weapon.transform.rotation *= RotationBackpack;
                PlayAnimation();
                return;
            }

            Vector2 cameraPosition = CameraTracker.WorldMousePos;
            Vector2 newPosition = _weapon.Owner.Rig.BoneHead.transform.position;
            Vector2 direction = cameraPosition - new Vector2(_weapon.transform.position.x, _weapon.transform.position.y);
            float distance = direction.magnitude;

            if(distance < 1f)//Точка вычисления направления оружия если дистанция до пивота оружия меньше 1
            {
                direction = cameraPosition - newPosition;
            }

            _weapon.Owner.Rig.RigBones2Ds[RigBone2D.LeftHand].rotation = _weaponUtility.HandGuardTransform.rotation;//пововрот левой руки равен прикладу
            _weapon.Owner.Rig.RigBones2Ds[RigBone2D.RightHand].rotation = _weaponUtility.GripTransform.rotation;//поворот правой руки равен рукояти

            _weapon.transform.rotation = Quaternion.LookRotation(Vector3.forward, new Vector3(-direction.y, direction.x, 0f));
            _weapon.transform.position = GetPositionWeapon(_weapon.transform.right.y);
            _weapon.transform.position -= GetRecoilVector(direction);
            PlayAnimation();
        }

        private Vector3 GetPositionWeapon(float time)
        {
            Quaternion rotation = _weapon.Owner.Rig.RigBones2Ds[RigBone2D.Body].transform.rotation;
            Vector3 origin = _weapon.Owner.Rig.RigBones2Ds[RigBone2D.Body].transform.position;
            Vector3 offset = new Vector3(_config.PositionX.Evaluate(time) * _weapon.Owner.CommonAction.ActionCharacterRotation.FlipBody, _config.PositionY.Evaluate(time), 0);
            Vector3 directionOrigin = new Vector3(0, offset.magnitude, 0f);
            Vector3 directionOffset = rotation * directionOrigin - directionOrigin;
            return offset + origin + directionOffset;
        }

        private Vector3 GetRecoilVector(Vector3 direction)
        {
            Vector3 moveRecoil  = Vector3.zero;
            if(_timeRecoil > 0)
            {
                float delta = _config.ForceRecoil.Evaluate(AnimationCurveTime - _timeRecoil);
                moveRecoil +=  direction.normalized * delta;
                _timeRecoil -= _deltaTime * Time.deltaTime;
            }
            return moveRecoil;
        }

        public void PlayRecoil(float time)
        {
            _timeRecoil = AnimationCurveTime;
            _deltaTime = AnimationCurveTime / time;
        }

        public void PlayArmingAnimationForward()
        {
            _weaponAnimator.Start
            (
                new List<List<AnimationState>>
                {
                    new() {AnimationState.HandForward},
                    new() {AnimationState.PositionForward, AnimationState.RotationForward}
                },
                new List<Action>
                {
                    () => {_weapon.IsTaken = true; Debug.Log($"{_weapon.IsTaken}");},
                    null
                }
            );
        }
        public void PlayArmingAnimationBackward()
        {
            _weaponAnimator.Start
            (
                new List<List<AnimationState>>
                {
                    new() {AnimationState.PositionBackward, AnimationState.RotationBackward},
                    new() {AnimationState.HandBackward}
                },
                new List<Action>
                {
                    () => {_weapon.IsTaken = false; Debug.Log($"{_weapon.IsTaken}");},
                    null
                }
            );
        }

        private void PlayAnimation()
        {
            List<AnimationState> states = _weaponAnimator.GetAnimation();
            if (states == null) return;

            foreach (var state in states)
            {   
                switch (state)
                {
                    case AnimationState.PositionBackward:
                        _weapon.transform.position = _weaponAnimator.WeaponAnimation.Animated(_weapon.transform.position, _weapon.Owner.Rig.Backpack.position);
                        break;
                    case AnimationState.PositionForward:
                        _weapon.transform.position = _weaponAnimator.WeaponAnimation.Animated(_weapon.Owner.Rig.Backpack.position, _weapon.transform.position);
                        break;
                    case AnimationState.RotationBackward:
                        _weapon.transform.rotation = _weaponAnimator.WeaponAnimation.Animated(_weapon.transform.rotation, _weapon.Owner.Rig.Backpack.rotation * RotationBackpack);
                        break;
                    case AnimationState.RotationForward:
                        _weapon.transform.rotation = _weaponAnimator.WeaponAnimation.Animated(_weapon.Owner.Rig.Backpack.rotation * RotationBackpack, _weapon.transform.rotation);
                        break;
                    case AnimationState.HandForward:
                        _weapon.Owner.Rig.LeftLimbSolver2D.weight = _weaponAnimator.WeaponAnimation.Animated(0, 1);
                        _weapon.Owner.Rig.RightLimbSolver2D.weight = _weaponAnimator.WeaponAnimation.Animated(0, 1);
                        break;
                    case AnimationState.HandBackward:
                        _weapon.Owner.Rig.LeftLimbSolver2D.weight = _weaponAnimator.WeaponAnimation.Animated(1, 0);
                        _weapon.Owner.Rig.RightLimbSolver2D.weight = _weaponAnimator.WeaponAnimation.Animated(1, 0);
                        break;
                }
            }
            _weaponAnimator.WeaponAnimation.Update();
        }
    }
}