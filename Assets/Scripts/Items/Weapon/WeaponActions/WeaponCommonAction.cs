using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class WeaponCommonAction : ICommonAction
    {
        [SerializeField] private int _currentClipCapacity;
        private Weapon _weapon;
        
        public int CurrentClipCapacity
        {
            get => _currentClipCapacity;
            set => _currentClipCapacity = value;
        }
        public WeaponRotation ActionWeaponRotation {get; private set;}
        public WeaponAttack Attack {get; private set;}
        public AmmoConfig[] AmmoTypeConfigs {get; private set;}
        public int CurrenAmmoType {get; set;}
        public ItemAnimator CommonAnimator {get; private set;} = new();

        public void Init(Weapon weapon, AmmoConfig[] ammoConfigs, int ammoType)
        {
            AmmoTypeConfigs = ammoConfigs;
            _weapon = weapon;
            Attack = new(_weapon, (WeaponCommonAction)_weapon.ItemCommonAction);
            ActionWeaponRotation = new(_weapon);
            _weapon.ActionsUpdate.Add(ActionWeaponRotation);
            CurrenAmmoType = ammoType;
        }
        public void Arming(bool arming)
        {
            if(arming == _weapon.IsTaken) 
            {
                CommonAnimator.End();
                return;
            }

            if(arming)
            {
                Debug.Log($"{_weapon.IsTaken}");
                ActionWeaponRotation.PlayArmingAnimationForward();
            }
            else
            {
                Debug.Log($"{_weapon.IsTaken}");
                ActionWeaponRotation.PlayArmingAnimationBackward();
            }
        }
    }
}