using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public enum AnimationState {None, Stop, PositionForward, PositionBackward, RotationForward, RotationBackward, HandForward, HandBackward}
    public class ItemAnimator
    {
        public event Action OnAnimationEnd;
        private float _time = 0.15f;
        private int _currentIndex;
        private List<List<AnimationState>> _playAnimationStates;
        private List<Action> _actionsOnEndAnimation;

        public ItemAnimation WeaponAnimation {get; private set;} = new();

        public ItemAnimator()
        {
            WeaponAnimation.OnAnimatedEnd += moveNext;
        }

        private void moveNext()
        {
            if(_currentIndex < _playAnimationStates.Count - 1) 
            {
                WeaponAnimation.Start(_time);
                _actionsOnEndAnimation[_currentIndex]?.Invoke();
                _currentIndex ++;
            }
            else
            {
                OnAnimationEnd?.Invoke();
                _playAnimationStates = null;
            }
        }

        public void Start(List<List<AnimationState>> playAnimationStates, List<Action> actions)
        {
            _actionsOnEndAnimation = actions;
            _playAnimationStates = playAnimationStates;
            _currentIndex = 0;
            WeaponAnimation.Start(_time);
        }

        public void End()
        {
            OnAnimationEnd?.Invoke();
        }

        public List<AnimationState> GetAnimation()
        {
            if(_playAnimationStates == null)
            {
                return null;
            }
            else
            {
                return _playAnimationStates[_currentIndex];
            }
        }
    }
}