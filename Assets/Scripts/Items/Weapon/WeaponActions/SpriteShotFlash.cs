using System.Collections;
using UnityEngine;

namespace U2D
{
    public class SpriteShotFlash : MonoBehaviour
    {
        public IEnumerator Flash(float time)
        {
            gameObject.SetActive(true);
            yield return new WaitForSeconds(time);
            gameObject.SetActive(false);
        }
    }
}