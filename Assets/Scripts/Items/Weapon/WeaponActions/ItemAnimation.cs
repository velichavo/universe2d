using System;
using UnityEngine;

namespace U2D
{
    public class ItemAnimation
    {
        public event Action OnAnimatedEnd;
        private bool _isAnimated;
        private float _timer;
        private float _deltaTime;


        public ItemAnimation()
        {

        }

        public void Start(float time)
        {
            _isAnimated = true;
            _timer = 1.0f;
            _deltaTime = _timer / time;
        }

        public void Update()
        {
            if(_timer > 0)
            {
                _timer -= _deltaTime * Time.deltaTime;
            }
            else
            {
                _isAnimated = false;
                OnAnimatedEnd?.Invoke();
            }
        }

        public Vector3 Animated(Vector3 start, Vector3 target)
        {

            return Vector3.Lerp(start, target, 1.0f - _timer);

        }
        public Quaternion Animated(Quaternion start, Quaternion target)
        {

            return Quaternion.Lerp(start, target, 1.0f - _timer);
        }
        public float Animated(float start, float target)
        {
            return Mathf.Lerp(start, target, 1.0f - _timer);
        }
    }
}