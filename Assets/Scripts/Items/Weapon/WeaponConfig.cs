using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "InventoryItemInfo", menuName = "Gameplay/Items/Create New Weapon")]
    public class WeaponConfig : CommonItemConfig
    {
        [SerializeField] private float _reloadTime;
        [SerializeField] private float _shootTime;
        [SerializeField] private float _startShootTime;
        [SerializeField] private float _endShootTime;
        [SerializeField] private float _flashTime;
        [SerializeField] private int _clipCapacity;
        [SerializeField] private float _damage;
        [SerializeField] private float _bulletSpeed;
        [SerializeField] AnimationCurve _forceRecoil;
        [SerializeField] private float _lengthRecoil;
        [SerializeField, Range(0, 1)] private float _accuracy;
        [SerializeField, Range(1, 10)] private int _bulletAmountPerShot;
        [SerializeField] private bool _isBurstShooting;
        [SerializeField] private bool _isPlayReloadSoundEqualClipCapacity;
        [SerializeField] private Vector3 _gripPositon;
        [SerializeField] private Vector3 _handguardPosition;
        [SerializeField] private Vector3 _clipPosition;
        [SerializeField] private Vector3 _shotFlashPosition;
        [SerializeField] private Sprite _spriteWeaponNoClip;
        [SerializeField] private Sprite _spriteClip;
        [SerializeField] private Sprite _spriteShotFlash;
        [SerializeField] private AnimationCurve _positionX;
        [SerializeField] private AnimationCurve _positionY;
        [SerializeField] private BulletType _bulletType;
        [SerializeField] private AudioClip _shootSound;
        [SerializeField] private AudioClip _reloadSound;
        [SerializeField] private AudioClip _startShootSound;
        [SerializeField] private AudioClip _endShootSound;
        [SerializeField] private AudioClip _noAmmoSound;

        public float ReloadTime => _reloadTime;
        public float ShootTime => _shootTime;
        public float StartShootTime => _startShootTime;
        public float EndShootTime => _endShootTime;
        public int ClipCapacity => _clipCapacity;
        public float Damage => _damage;
        public float Speed => _bulletSpeed;
        public float Accuracy => _accuracy;
        public AnimationCurve ForceRecoil => _forceRecoil;
        public int BulletAmountPerShot => _bulletAmountPerShot;
        public bool IsBurstShooting => _isBurstShooting;
        public bool IsPlayReloadSoundEqualClipCapacity => _isPlayReloadSoundEqualClipCapacity;
        public Sprite SpriteWeaponNoClip => _spriteWeaponNoClip;
        public Sprite SpriteShotFlash =>_spriteShotFlash;
        public float FlashTime => _flashTime;
        public Vector3 GripPositon => _gripPositon;
        public Vector3 HandguardPosition => _handguardPosition;
        public BulletType WeaponBulletType => _bulletType;
        public AudioClip ShootSound => _shootSound;
        public AudioClip ReloadSound => _reloadSound;
        public AudioClip StartShootSound => _startShootSound;
        public AudioClip EndShootSound => _endShootSound;
        public AudioClip NoAmmoSound => _noAmmoSound;
        public Sprite SpriteClip => _spriteClip;
        public Vector3 SpriteClipPosition => _clipPosition;
        public Vector3 ShotFlashPosition => _shotFlashPosition;
        public AnimationCurve PositionX => _positionX;
        public AnimationCurve PositionY => _positionY;
    }
}