using UnityEngine;

namespace U2D
{
    [ExecuteAlways]
    public class WeaponAnimatePosition : MonoBehaviour
    {
        [SerializeField] private Player _character;
        [SerializeField] private WeaponConfig _config;
        [SerializeField] private float _eulerRotatoinZ;
        [SerializeField] private Vector2 _direction;
        [SerializeField] private bool _isSetPosition;
        [SerializeField] AnimationCurve animationCurveX;
        [SerializeField] AnimationCurve animationCurveY;

        public WeaponConfig Config => _config;

        public void Init()
        {
            
        }

        public void Update()
        {
            transform.rotation = Quaternion.Euler(new(0, 0, _eulerRotatoinZ));
            _direction = transform.right;
            if(_character)
                CharacterRotation.BodyRotation(_character, _direction, 1f);
            SetPositionWeapon(_direction.y);
        }

        private void SetPositionWeapon(float y)
        {
            if(_isSetPosition)
            {
                transform.localPosition = new(animationCurveX.Evaluate(y), animationCurveY.Evaluate(y));
            }
        }


        public void AddPositionToCurves()
        {
            int x = animationCurveX.AddKey(_direction.y, transform.localPosition.x);
            int y = animationCurveY.AddKey(_direction.y, transform.localPosition.y);
            Debug.Log($"Key add, time: {_direction.y}, Position: {transform.localPosition.x}, {transform.localPosition.y}");
        }

        public void ClearKeys()
        {
            animationCurveX = new();
            animationCurveY = new();
        }

        public void SetAnimationCurveFromConfig()
        {
            animationCurveX = _config.PositionX;
            animationCurveY = _config.PositionY;
        }
    }
}