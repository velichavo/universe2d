using UnityEngine;

namespace U2D
{
    public class WeaponUtility : CommonItemUtility
    {
        private Weapon _weapon;
        public Transform HandGuardTransform {get; private set;}
        public Transform GripTransform {get; private set;}
        public Transform EjectorTransform {get; private set;}
        public Transform ShotFlashTransform {get; private set;}
        public SpriteShotFlash SpriteShotFlash {get; private set;}


        public WeaponUtility(Weapon weapon) : base(weapon)
        {
            _weapon = weapon;
            ApplyPositionChildTransform();
        }

        private void ApplyPositionChildTransform()
        {
            foreach (var item in _weapon.GetComponentsInChildren<Transform>())
            {
                switch(item.gameObject.name)
                {
                    case "HandGuard":
                        item.localPosition = _weapon.Config.HandguardPosition;
                        HandGuardTransform = item;
                        item.gameObject.SetActive(false);
                        break;
                    case "Grip":
                        item.localPosition = _weapon.Config.GripPositon;
                        GripTransform = item;
                        item.gameObject.SetActive(false);
                        break;
                    case "Ejector":
                        item.localPosition = _weapon.Config.SpriteClipPosition;
                        EjectorTransform = item;
                        item.gameObject.SetActive(false);
                        break;
                    case "ShotFlash":
                        item.localPosition = _weapon.Config.ShotFlashPosition;
                        ShotFlashTransform = item;
                        SpriteShotFlash = item.GetComponent<SpriteShotFlash>();
                        SpriteRenderer sr = item.GetComponent<SpriteRenderer>();
                        sr.sprite = _weapon.Config.SpriteShotFlash;
                        item.gameObject.SetActive(false);
                        break;
                }
            }
        }
    }
}