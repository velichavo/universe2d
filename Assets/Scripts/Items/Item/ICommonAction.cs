namespace U2D
{ 
    public interface ICommonAction
    {
        public int CurrentClipCapacity {get; set;}
        public AmmoConfig[] AmmoTypeConfigs {get;}
        public int CurrenAmmoType {get; set;}
        public ItemAnimator CommonAnimator {get;}

        /// <summary>
        ///Если держим в руках
        ///1. проигрываем анимацию убирания за спину.
        ///2. Плавно убираем вес в инверсной кинематики до 0
        ///иначе все делаем в обратном порядке
        /// </summary>
        /// <param name="arming"></param>
        public void Arming(bool arming);
    }
}