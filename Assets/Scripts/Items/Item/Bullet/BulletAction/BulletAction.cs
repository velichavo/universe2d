using UnityEngine;

namespace U2D 
{
    public class BulletAction
    {
        private System.Action<CommonBullet> _callBack;
        
        private bool _isCollision;
        private float _speed;
        private float _damage;
        private float _bounceCount;
        private Vector3 _direction;
        private Vector3 _movement;
        private CommonBullet _bullet;
        private Weapon _weapon;
        private GameObject _parent;
        private RaycastHit2D[] _hit2Ds;
        public BulletAction(CommonBullet bullet)
        {
            _bullet = bullet;
        }

        public void PoolFixedUpdate()
        {
            if(_isCollision) 
            {
                OnCollision();
                return;
            }

            _hit2Ds = Physics2D.RaycastAll(_bullet.transform.position, _direction, _speed * Time.fixedDeltaTime, _bullet.Config.CollisionMask);
            for(int i = 0; i < _hit2Ds.Length; i++)
            {
                if(_hit2Ds[i])
                {
                    IEntityCollision entity = _hit2Ds[i].transform.GetComponent<IEntityCollision>();
                    if(entity == null) return;
                    if(ReferenceEquals(_parent, entity.CollisionGameobject)) continue;
                    _bullet.transform.position += _direction * _hit2Ds[i].distance;
                    _bullet.transform.rotation = Quaternion.FromToRotation(Vector3.right, _hit2Ds[i].normal);
                    _bullet.TrailRendererBullet.time = _bullet.Config.TimeTrailMove;
                    PlayEffect(entity.SurfaceType);
                    entity.OnBulletCollision(_damage);
                    _isCollision = true;
                    return;
                }
            }
            _bullet.transform.position += _movement * Time.fixedDeltaTime;
        }

        private void PlayEffect(SurfaceTypeMaterial type)
        {
            switch (_bullet.Config.CommonBulletType)
            {
                case BulletType.Default:
                    PlayDefault(type);
                    break;
                case BulletType.Explosion:
                    PlayExplosion();
                    break;
            }
        }

        private void PlayExplosion()
        {
            throw new System.NotImplementedException();
        }

        private void PlayDefault(SurfaceTypeMaterial type)
        {
            PoolEffect poolEffect = _bullet.World.Pools.GetFXPoolFromSurface(type);
            poolEffect.Pool.Pool.Get().FXPlay(_bullet.transform.position, _bullet.transform.rotation);
            _bullet.Audio.PlayOneShot(poolEffect.Sound);
        }

        private bool Bounce()
        {

            return true;
        }

        internal void Start(Weapon weapon, System.Action<CommonBullet> callBack)
        {
            _weapon = weapon;
            _callBack = callBack;
            _parent = _weapon.Owner.gameObject;
            
            _isCollision = false;

            float accuracy = (Random.Range(0, 2f) - 1) * (1 - _weapon.Config.Accuracy);
            _direction = _weapon.transform.right;
            _direction += new Vector3(-_direction.y * accuracy, _direction.x * accuracy, 0);
            _direction = _direction.normalized;
            _bullet.transform.rotation = Quaternion.FromToRotation(Vector3.right, _direction);

            _speed = _weapon.Config.Speed;
            _damage = _weapon.Config.Damage;
            _movement = _direction * _speed;
            _bounceCount = _bullet.Config.BounceCount;

            _bullet.transform.position = _bullet.transform.rotation * _weapon.Config.ShotFlashPosition + weapon.transform.position;
            _bullet.TrailRendererBullet.time = _bullet.Config.TimeTrailMove;
        }

        private void OnCollision()
        {
            if(_bullet.TrailRendererBullet.positionCount == 0)
            {
                if(_callBack != null)
                {
                    _callBack(_bullet);
                }
                else
                {
                    Debug.LogError($"Метод возврата в пул не существует");
                }
            }
        }
    }
}