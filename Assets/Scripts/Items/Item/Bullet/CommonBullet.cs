using System;
using NTC.Singleton;
using UnityEngine;

namespace U2D
{
    public enum BulletType {None, Default, Bounce, Explosion}
    public class CommonBullet : MonoBehaviour
    {
        [SerializeField] private AmmoConfig _config;

        private BulletAction _action;

        public AmmoConfig Config => _config;
        public TrailRenderer TrailRendererBullet {get; private set;}
        public AudioSource Audio {get; private set;}
        public WorldCore World {get; private set;}

        public CommonBullet Init()
        {
            _action = new(this);
            TrailRendererBullet = GetComponent<TrailRenderer>();
            Audio = GetComponent<AudioSource>();
            World = Singleton<WorldCore>.Instance;
            return this;
        }

        public void BulletStart(AmmoConfig config, Weapon weapon, Action<CommonBullet> callBack)
        {
            _config = config;
            _action.Start(weapon, callBack);
        }

        public void PoolFixedUpdate()
        {
            _action.PoolFixedUpdate();
        }
    }
}