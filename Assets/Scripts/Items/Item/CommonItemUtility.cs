using UnityEngine;

namespace U2D
{
    public class CommonItemUtility : IItemUtility
    {
        public Collider2D Collider {get; protected set;}
        public Rigidbody2D Rb2d {get; protected set;}
        public AudioSource Audio {get; protected set;}

        public CommonItemUtility(CommonItem item)
        {
            Collider = item.GetComponent<Collider2D>();
            Rb2d = item.GetComponent<Rigidbody2D>();
            Audio = item.GetComponent<AudioSource>();
        }
    }
}