using UnityEngine;

namespace U2D
{
    public class CommonItemConfig : ScriptableObject
    {
        [SerializeField] private string _itemType;
        [SerializeField] private string _title;
        [SerializeField] private string _description;
        [SerializeField] private float _weight;
        [SerializeField] private int _amount;
        [SerializeField] private bool _isCanEquip;
        [SerializeField] private bool _eachInNewSlot;
        [SerializeField] private Sprite _iconInInventory;
        [SerializeField] private Sprite _spriteItem;
        [SerializeField] private GameObject _prefab;
        [SerializeField] private Vector2[] _colliderPoints;
        [SerializeField] private AudioClip[] _soundThrow;
        [SerializeField] private AudioClip[] _soundImpact;
        [SerializeField] private AudioClip[] _soundPickUp;
        [SerializeField] private AudioClip[] _soundTake;
        [SerializeField] private AudioClip[] _soundEquip;
        [SerializeField] private AmmoConfig[] _ammoTypeConfig;
        [SerializeField] private int _defaultAmmoType;
 
        public string ItemType => _itemType;
        public string Title => _title;
        public string Description => _description;
        public bool EachInNewSlot => _eachInNewSlot;
        public bool IsCanEquip => _isCanEquip;
        public Sprite IconInInventory => _iconInInventory;
        public Sprite SpriteItem => _spriteItem;
        public float Weight => _weight;
        public int Amount => _amount;
        public GameObject Prefab => _prefab;
        public Vector2[] ColliderPoints => _colliderPoints;
        public AudioClip[] SoundThrow => _soundThrow;
        public AudioClip[] SoundImpact => _soundImpact;
        public AudioClip[] SoundPickUp => _soundPickUp;
        public AudioClip[] SoundTake => _soundTake;
        public AudioClip[] SoundEquip => _soundEquip;
        public AmmoConfig[] AmmoTypeConfig => _ammoTypeConfig;
        public int DefaultAmmoType => _defaultAmmoType;
    }
}