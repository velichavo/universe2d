using UnityEngine;

namespace U2D
{
    public interface ICommonItem
    {
        bool IsTaken {get; set;}
        int Amount {get;}
        Player Owner {get;}
        CommonItemConfig ItemConfig {get;}
        ICommonAction ItemCommonAction {get;}
        void Action(GetKeyState state);
        void PickUp(Player character);
        void Drop();
        void Equip(bool isEquipped);
        void Take(bool isTaken);
        void ItemDestroy();
    }
}