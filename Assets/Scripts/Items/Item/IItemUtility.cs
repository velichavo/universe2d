using UnityEngine;

namespace U2D
{
    public interface IItemUtility
    {
        Collider2D Collider {get;}
        Rigidbody2D Rb2d {get;}
        AudioSource Audio {get;}
    }
}