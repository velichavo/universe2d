
using UnityEngine;

namespace U2D
{
    public abstract class CommonItem : Entity, ICommonItem
    {
        public abstract CommonItemConfig ItemConfig { get; }
        public abstract int Amount { get; }
        public virtual Player Owner { get; private set;}
        public bool IsTaken { get ; set; }
        public abstract CommonItemUtility Utility {get;}

        public abstract ICommonAction ItemCommonAction {get;}

        public virtual void Action(GetKeyState state)
        {
        }

        public virtual void Drop()
        {
            Utility.Audio.PlayOneShot(GetRandomAudioClip(ItemConfig.SoundThrow));
            Utility.Collider.enabled = true;
            Utility.Rb2d.simulated = true;
            Owner = null;
            gameObject.SetActive(true);
        }

        public virtual void PickUp(Player character)
        {
            character.CommonAction.Audio.PlayOneShot(GetRandomAudioClip(ItemConfig.SoundPickUp));
            Utility.Collider.enabled = false;
            Utility.Rb2d.simulated = false;
            Owner = character;
            gameObject.SetActive(false);
        }

        public virtual void Take(bool isTaken)
        {
            //IsTaken = isTaken;
            Utility.Audio.PlayOneShot(GetRandomAudioClip(ItemConfig.SoundTake));
        }

        public virtual void ItemDestroy()
        {
            Destroy(this);
        }

        public override string ToString()
        {
            return ItemConfig.Title;
        }

        public virtual void Equip(bool isEquipped)
        {
            gameObject.SetActive(isEquipped);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Utility.Audio.PlayOneShot(GetRandomAudioClip(ItemConfig.SoundImpact));
        }

        private AudioClip GetRandomAudioClip(AudioClip[] clips)
        {
            return clips[(int)(Random.value * ItemConfig.SoundImpact.Length)];
        } 
     }
}