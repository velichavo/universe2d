using UnityEngine;
using UnityEditor;
using Unity.Mathematics;
using System;

namespace U2D
{
    [System.Serializable]
    public class CameraTracker
    {
        public event Action<Vector3> OnChangedCameraPosition;
        [SerializeField] float cameraSpeed = 1;
        [SerializeField, Range(0, .5f)] float cameraRange = .3f;
        [SerializeField] private float OutOfRange = 1f;
        private readonly Player _player;
        private readonly Camera _camera;
        private Bounds _cameraBounds;

        public float CameraSpeed => cameraSpeed;
        public float CameraRange => cameraRange;
        public Bounds CameraBounds => _cameraBounds;
        public static Vector3 WorldMousePos {get; private set ;}
        public static Vector3 DirectionToMouse {get; private set;}
        public CameraTracker(CameraTracker cameraTracker, Camera camera, Player player)
        {
            _camera = camera;
            _player = player;
            cameraSpeed = cameraTracker.cameraSpeed;
            cameraRange = cameraTracker.CameraRange;
        }

        public void Update()
        {
            MouseToWorldPoint();
            Vector3 direction = WorldMousePos - _player.transform.position;
            DirectionToMouse = new Vector3(direction.x * cameraRange, direction.y * cameraRange, 0);
            _camera.transform.position = Vector3.Lerp(_camera.transform.position, new Vector3(DirectionToMouse.x, DirectionToMouse.y, _camera.transform.position.z) + _player.transform.position, cameraSpeed * Time.deltaTime);
            OnChangedCameraPosition?.Invoke(_camera.transform.position);
            Vector3 min = _camera.ViewportToWorldPoint(new Vector3(0, 0, _camera.nearClipPlane));   //Получаем вектор нижнего левого угла камеры в мировых координатах
            Vector3 max = _camera.ViewportToWorldPoint(new Vector3(1f, 1f, _camera.nearClipPlane));   //Получаем верхний правый угол камеры в мировых координатах
            _cameraBounds.SetMinMax(min, max);
        }    
        private void MouseToWorldPoint()
        {
            Vector3 cameraPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            WorldMousePos = new(cameraPosition.x, cameraPosition.y, 0);
        }

        public Vector3 WorldToScreenPoint(Vector3 worldPosition)
        {  
            return _camera.WorldToScreenPoint(worldPosition);
        }

        public Vector3 GetCameraPosition()
        {
            return _camera.transform.position;
        }

        public void OnGUI()
        {
            float isRect = math.abs(WorldMousePos.x - CameraBounds.min.x) + math.abs(CameraBounds.max.x - WorldMousePos.x);
            Handles.Label(new Vector3(100, 100, 0), $"isRect: {isRect}, WorldMousePos: {CameraBounds.max.x - CameraBounds.min.x}, CameraBoundsMinX: {CameraBounds.min.x}, CameraBoundsMaxX: {CameraBounds.max.x}");
        }
    }
}
