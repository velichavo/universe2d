
namespace U2D
{
    public class EntitiesInitializer
    {
        public void Init(Entity[] entities)
        {
            foreach (var entity in entities)
            {
                entity.Init();
            }
        }
    }
}
