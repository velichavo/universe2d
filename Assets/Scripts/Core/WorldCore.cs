using NTC.MonoCache;
using NTC.Singleton;
using UnityEngine;

namespace U2D
{
    public enum GameState {None, Play, Pause, InventoryMenu}
    [DefaultExecutionOrder(-3)]
    public class WorldCore : Singleton<WorldCore>, IWorldCore
    {
        [SerializeField] private Player[] _players;
        [SerializeField] private Camera _camera;
        [SerializeField] private CameraTracker _cameraTracker;
        [SerializeField] private PoolsManagerConfig _poolsManagerConfig;
        [SerializeField] private ParalaxController _paralaxController;
        private InventoryUI _inventoryUI;
        private HUD _bottomUI;

        public CommonItem[] CommonItems {get; private set;}
        public Player[] Characters {get; private set;}
        public Entity[] Entities {get; private set;}
        public GlobalUpdate MonoGlobalUpdate {get; private set;}
        public PoolsManager Pools {get; private set;}
        public ObservedValue<GameState> GlobalGameState {get; private set;} = new();
        //Main start
        protected override void OnAwake()
        {
            Application.targetFrameRate = int.MaxValue;
            MonoGlobalUpdate = new();
            Pools = new(_poolsManagerConfig, this);
#if DEBUG
            MonoCacheExceptionsDetector.CheckForExceptions();
#endif
        }

        private void Start()//инициализация должна быть после инициализации всех объектов
        {
            _cameraTracker = new(_cameraTracker, _camera, _players[0]);
            _inventoryUI = GetComponentInChildren<InventoryUI>();
            _inventoryUI.Init(() => _players[0].ActionItem);
            _bottomUI = GetComponentInChildren<HUD>();
            _bottomUI.Init(_players[0].ActionItem);

            GlobalGameState.Value = GameState.Play;
            _players[0].ActionItem.UpdateHUD();

            _paralaxController.Init(_cameraTracker);
        }

        //Main cicle
        private void Update()
        {
            switch (GlobalGameState.Value)
            {
                case GameState.Play:
                    _cameraTracker.Update();
                    MonoGlobalUpdate.MonoUpdate();
                    break;
                case GameState.InventoryMenu:
                    _inventoryUI.CustomUpdate();
                    break;
            }
        }
        private void FixedUpdate()
        {
            switch (GlobalGameState.Value)
            {
                case GameState.Play:
                    MonoGlobalUpdate.MonoFixedUpdate();
                    Pools.GlobalPoolBullet.PoolBulletFixedUpdate();
                    break;
            }
            
        }

        private void LateUpdate()
        {
            switch (GlobalGameState.Value)
            {
                case GameState.Play:
                    MonoGlobalUpdate.MonoLateUpdate();
                    break;
            }
        }

        /*private void OnGUI()
        {
            _cameraTracker.OnGUI();
        }*/
    }
}