using System.Collections.Generic;
using NTC.MonoCache;
using NTC.Singleton;
using UnityEngine;

namespace U2D
{
    public abstract class Entity : MonoBehaviour, IEntity
    {
        public List<IActionFixedUpdate> ActionsFixedUpdate {get; private set;} = new();
        public List<IActionUpdate> ActionsUpdate {get; private set;} = new();
        public List<IActionLateUpdate> ActionsLateUpdate {get; private set;} = new();


        private void Awake()
        {
            Init();
        }
        public virtual void Init()
        {
        }

        internal int _index;

        private GlobalUpdate _globalUpdate;
        private WorldCore _worldCore;
        private bool _isSetup;
        
        public WorldCore World => _worldCore;
        private void OnEnable()
        {
            OnEnabled();

            if (_isSetup == false)
            {
                TrySetup();
            }

            if (_isSetup)
            {
                _globalUpdate.Add(this);
            }
        }

        private void OnDisable()
        {
            if (_isSetup)
            {
                _globalUpdate.Remove(this);
            }

            OnDisabled();
        }

        private void TrySetup()
        {
            if (Application.isPlaying)
            {
                _globalUpdate = Singleton<WorldCore>.Instance.MonoGlobalUpdate;
                _worldCore = Singleton<WorldCore>.Instance;
                _isSetup = true;
            }
            else
            {
                throw new System.Exception(
                    $"You tries to get {nameof(WorldCore)} instance when application is not playing!");
            }
        }

        internal void RaiseRun() => MonoUpdate();
        internal void RaiseFixedRun() => MonoFixedUpdate();
        internal void RaiseLateRun() => MonoLateUpdate();

        protected virtual void OnEnabled() { }
        protected virtual void OnDisabled() { }
        protected virtual void MonoUpdate() 
        {
            foreach (var item in ActionsUpdate)
            {
                item.CustomUpdate();
            }
        }
        protected virtual void MonoFixedUpdate()
        {
            foreach (var item in ActionsFixedUpdate)
            {
                item.CustomFixedUpdate();
            }
        }
        protected virtual void MonoLateUpdate()
        {
            foreach (var item in ActionsLateUpdate)
            {
                item.CustomLateUpdate();
            }
        }
    }
}