using UnityEngine;

namespace U2D
{
    public interface IEntity
    {
        void Init();
    }
}