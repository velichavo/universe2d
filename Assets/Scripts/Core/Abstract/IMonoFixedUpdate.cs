namespace U2D
{
    public interface IMonoFixedUpdate
    {
        public void MonoFixedUpdate();
    }
}