using UnityEngine;

namespace U2D
{
    public enum SurfaceTypeMaterial {None, Body, SoftBody, Metal, Concrete, Sand, Dirt, Wood}
    public interface IEntityCollision
    {
        SurfaceTypeMaterial SurfaceType {get;}
        GameObject CollisionGameobject {get;}
        void OnBulletCollision(float damage);
    }
}