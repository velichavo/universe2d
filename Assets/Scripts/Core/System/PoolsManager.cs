using UnityEngine;

namespace U2D
{
    [System.Serializable]
    public class PoolsManager
    {
        public PoolsManagerConfig Config {get; private set;}
        public BulletPool GlobalPoolBullet {get; private set;}
        public ClipPool GlobalPoolClip {get; private set;}
        public FXPool GlobalPoolFXBody {get; private set;}
        public FXPool GlobalPoolFXConcrete {get; private set;}
        public FXPool GlobalPoolFXDirt {get; private set;}
        public FXPool GlobalPoolFXMetal {get; private set;}
        public FXPool GlobalPoolFXSand {get; private set;}
        public FXPool GlobalPoolFXSoftbody {get; private set;}
        public FXPool GlobalPoolFXWood {get; private set;}
        public FXPool GlobalPoolFXExpolsion {get; private set;}

        public PoolsManager(PoolsManagerConfig config, WorldCore world)
        {
            Config = config;
            GlobalPoolBullet = world.GetComponentInChildren<BulletPool>().Init();
            GlobalPoolClip = world.GetComponentInChildren<ClipPool>().Init();

            FXPool[] PoolsFX = world.GetComponentsInChildren<FXPool>(); 
            foreach (var item in PoolsFX)
            {
                item.Init();

                switch (item.Config.PoolType)
                {
                    case CommonFXType.Body:
                        GlobalPoolFXBody = item;
                        break;
                    case CommonFXType.Concrete:
                        GlobalPoolFXConcrete = item;
                        break;
                    case CommonFXType.Dirt:
                        GlobalPoolFXDirt = item;
                        break;
                    case CommonFXType.Metal:
                        GlobalPoolFXMetal = item;
                        break;
                    case CommonFXType.Sand:
                        GlobalPoolFXSand = item;
                        break;
                    case CommonFXType.SmallExplosion:
                        GlobalPoolFXExpolsion = item;
                        break;
                    case CommonFXType.SoftBody:
                        GlobalPoolFXSoftbody = item;
                        break;
                    case CommonFXType.Wood:
                        GlobalPoolFXWood = item;
                        break;
                    default:
                        Debug.Log("Неизвестный тип пула");
                        break;
                }
            }
        }

        public PoolEffect GetFXPoolFromSurface(SurfaceTypeMaterial type)
        {
            switch (type)
            {
                case SurfaceTypeMaterial.Body:
                    return new(GlobalPoolFXBody, Config.SoundImpact[(int)type]);
                case SurfaceTypeMaterial.Concrete:
                    return new(GlobalPoolFXConcrete, Config.SoundImpact[(int)type]);
                case SurfaceTypeMaterial.Dirt:
                    return new(GlobalPoolFXDirt, Config.SoundImpact[(int)type]);
                case SurfaceTypeMaterial.Metal:
                    return new(GlobalPoolFXMetal, Config.SoundImpact[(int)type]);
                case SurfaceTypeMaterial.Sand:
                    return new(GlobalPoolFXSand, Config.SoundImpact[(int)type]);
                case SurfaceTypeMaterial.SoftBody:
                    return new(GlobalPoolFXSoftbody, Config.SoundImpact[(int)type]);
                case SurfaceTypeMaterial.Wood:
                    return new(GlobalPoolFXWood, Config.SoundImpact[(int)type]);
                default:
                    return new(GlobalPoolFXConcrete, Config.SoundImpact[0]);
            }
        }
    }

    public class PoolEffect
    {
        public FXPool Pool {get; private set;}
        public AudioClip Sound {get; private set;}

        public PoolEffect(FXPool pool, AudioClip sound)
        {
            Pool = pool;
            Sound = sound;
        }
    }   
}