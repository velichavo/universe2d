using UnityEngine;

namespace U2D
{
    public class GameObjectPool<T> : MonoBehaviour
    {
        [SerializeField] protected PoolConfig _config;
        public PoolBase<T> Pool {get; private set;}
        public PoolConfig Config => _config;
        public void Init()
        {
            Pool = new(Preload, GetAction, ReturnAction, _config.CountObject);
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }

        public virtual T Preload()
        {
            T item = default;
            return item;
        }
        public virtual void GetAction(T bullet)
        {

        }
        public virtual void ReturnAction(T bullet)
        {

        }

    }
}