using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "PoolsManagerConfig", menuName = "Gameplay/Pools/Create New PoolsManagerConfig")]
    public class PoolsManagerConfig : ScriptableObject
    {
        [SerializeField] private AudioClip[] _soundImpact = new AudioClip[10];//Для каждого звука сделать отдельную переменную

        public AudioClip[] SoundImpact => _soundImpact;
    }
}