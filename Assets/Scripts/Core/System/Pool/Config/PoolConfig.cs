using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "PoolConfig", menuName = "Gameplay/Pools/Create New PoolConfig")]
    public class PoolConfig : ScriptableObject
    {
        [SerializeField] private CommonFXType _poolType;
        [SerializeField] private GameObject _prefabullet;
        [SerializeField] private int _countObject;

        public GameObject PrefabObject => _prefabullet;
        public int CountObject => _countObject;
        public CommonFXType PoolType => _poolType;
    }
}