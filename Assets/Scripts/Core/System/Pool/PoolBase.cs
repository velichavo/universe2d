///Leksay's Development

using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public class PoolBase<T>
    {
        private readonly Func<T> _preloadFunc;
        private readonly Action<T> _getAction;
        private readonly Action<T> _returnAction;
        private Queue<T> _pool = new Queue<T>();
        private List<T> _active = new List<T>();

        public List<T> Actives => _active;

        public PoolBase(Func<T> preloadFunc, Action<T> getAction, Action<T> returnAction, int preloadCount)
        {
            _preloadFunc = preloadFunc;
            _getAction = getAction;
            _returnAction = returnAction;
            if (preloadFunc == null)
            {
                Debug.LogError("Preload fuction is null");
                return;
            }
            for (int i = 0; i < preloadCount; i++)
            {
                Return(_preloadFunc());
            }
        }

        public T Get()
        {
            T item = _pool.Count > 0 ? _pool.Dequeue() : _preloadFunc();
            _getAction(item);
            _active.Add(item);
            return item;
        }
        
        public void Return(T item)
        {
            _returnAction(item);
            _pool.Enqueue(item);
            _active.Remove(item);
        }

        public void returnAll()
        {
            foreach (T item in _active)
            {
                Return(item);
            }
        }

    }
}