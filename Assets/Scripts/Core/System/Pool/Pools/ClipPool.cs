using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace U2D
{
    public class ClipPool : GameObjectPool<CommonClip>
    {
        public new ClipPool Init()
        {
            base.Init();
            return this;
        }

        public override CommonClip Preload()
        {
            CommonClip clip = Instantiate(_config.PrefabObject.GetComponent<CommonClip>());
            clip.Init((clip) => Pool.Return(clip));
            clip.transform.parent = transform;
            return clip;
        }
        public override void GetAction(CommonClip clip) => clip.gameObject.SetActive(true);
        public override void ReturnAction(CommonClip clip) => clip.gameObject.SetActive(false);
    }
}