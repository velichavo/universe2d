using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public class BulletPool : GameObjectPool<CommonBullet>
    {
        public new BulletPool Init()
        {
            base.Init();
            return this;
        }

        public override CommonBullet Preload()
        {
            CommonBullet bullet = Instantiate(_config.PrefabObject.GetComponent<CommonBullet>()).Init();
            bullet.transform.parent = transform;
            return bullet;
        }
        public override void GetAction(CommonBullet bullet) => bullet.gameObject.SetActive(true);
        public override void ReturnAction(CommonBullet bullet) => bullet.gameObject.SetActive(false);

        public void PoolBulletFixedUpdate()
        {
            for(int i = 0; i < Pool.Actives.Count; i++)
            {
                Pool.Actives[i].PoolFixedUpdate();
            }
        }

    }
}