using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public class FXPool : GameObjectPool<CommonFX>
    {   
        public new FXPool Init()
        {
            base.Init();
            return this;
        }

        public override CommonFX Preload()
        {
            CommonFX fx = Instantiate(_config.PrefabObject.GetComponent<CommonFX>()).Init(this);
            fx.transform.parent = transform;
            return fx;
        }
        public override void GetAction(CommonFX fx) => fx.gameObject.SetActive(true);
        public override void ReturnAction(CommonFX fx) => fx.gameObject.SetActive(false);

    }
}