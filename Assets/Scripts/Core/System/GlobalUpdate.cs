// -------------------------------------------------------------------------------------------
// The MIT License
// MonoCache is a fast optimization framework for Unity https://github.com/MeeXaSiK/MonoCache
// Copyright (c) 2021-2023 Night Train Code
// -------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using U2D;

namespace NTC.MonoCache
{
    public sealed class GlobalUpdate
    {
        private readonly List<Entity> _monoCaches = new List<Entity>(32);
        private int _count;

        public void MonoUpdate()
        {
            for (int i = 0; i < _count; i++)
            {
                _monoCaches[i].RaiseRun();
            }
        }

        public void MonoFixedUpdate()
        {
            for (int i = 0; i < _count; i++)
            {
                _monoCaches[i].RaiseFixedRun();
            }
        }

        public void MonoLateUpdate()
        {
            for (int i = 0; i < _count; i++)
            {
                _monoCaches[i].RaiseLateRun();
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Add(Entity monoCache)
        {
            _monoCaches.Add(monoCache);
            monoCache._index = _count++;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Remove(Entity monoCache)
        {
            var lastComponent = _monoCaches[_count-1];
            _monoCaches[monoCache._index] = lastComponent;
            lastComponent._index = monoCache._index;
            _monoCaches.RemoveAt(--_count);
        }
    }
}