using System;
using UnityEngine;
using System.Linq;

[Serializable]
public class ObservedValue<T>
{
    public event Action<object> OnChanged;

    [SerializeField] private T _value;
    public T Value
    {
        get 
        {
            return _value;
        }
        set
        {
            _value = value;
            OnChanged?.Invoke(value);
        }
    }

    public ObservedValue()
    {
        Value = default;
    }

    public ObservedValue(T dafaultValue)
    {
        Value = dafaultValue;
    }

    public override string ToString()
    {
        return Value.ToString();
    }

    public bool IsSubscribe(Action<object> obj)
    {
        if(obj != null)
            return Array.Exists(OnChanged.GetInvocationList(),  ( ev ) => obj.Equals ( ev ));
        else
            return false;
    }
}