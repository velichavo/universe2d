using System;
using UnityEngine;

namespace U2D
{
    public class ParalaxController : MonoBehaviour
    {
        private BackgroundLayer[] _paralaxLayers;
        private CameraTracker _camera;

        public void Init(CameraTracker camera)
        {
            _camera = camera;
            _camera.OnChangedCameraPosition += ParalaxUpdate;
            _paralaxLayers = GetComponentsInChildren<BackgroundLayer>();
            foreach (var layer in _paralaxLayers)
            {
                layer.Init(camera.GetCameraPosition());
            }
        }

        public void ParalaxUpdate(Vector3 position)
        {
            foreach (var layer in _paralaxLayers)
            {
                layer.LayerUpdate(position);
            }
        }

        private void OnDestroy()
        {
            _camera.OnChangedCameraPosition -= ParalaxUpdate;
        }
    }
}