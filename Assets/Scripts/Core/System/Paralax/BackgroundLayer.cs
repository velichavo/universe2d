using UnityEngine;

namespace U2D
{
    public class BackgroundLayer : MonoBehaviour
    {
        [SerializeField, Range(0, 1)] private float _paralaxStrength; 
        [SerializeField] private bool _disableVerticalParalax;
        private Vector3 _targetPreviousPosition;

        public void Init(Vector3 position)
        {
            _targetPreviousPosition = position;
        }

        public void LayerUpdate(Vector3 position)
        {
            Vector3 delta = position - _targetPreviousPosition;
            _targetPreviousPosition = position;
            if(_disableVerticalParalax)
                delta.y = 0;
            transform.position += delta * _paralaxStrength;
        }
    }
}