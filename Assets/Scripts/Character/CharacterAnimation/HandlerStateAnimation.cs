using UnityEngine;

namespace U2D
{
    public class HandlerStateAnimation
    {
        private Player _character;
        private AnyStateAnimator _stateAnimator;
        public HandlerStateAnimation(AnyStateAnimator stateAnimator, Player character)
        {
            if(character.CommonAction == null)
            {
                Debug.LogError("CommonAction not exist");
                return;
            }
            _character = character;
            _character.CommonAction.ChangeStateAction += AnimationChange;
            _stateAnimator = stateAnimator;
        }   
        private void AnimationChange(StateAction state)
        {
            switch(state)
            {
                case StateAction.Idle:
                    PlayAnimation(AnimationVariables.body_idle, AnimationVariables.legs_idle);
                    return;
                case StateAction.RunForward:
                    PlayAnimation(AnimationVariables.body_slow_run_forward, AnimationVariables.legs_slow_run_forward);
                    return;
                case StateAction.RunBack:
                    PlayAnimation(AnimationVariables.body_slow_run_back, AnimationVariables.legs_slow_run_back);
                    return;
                case StateAction.Jump:

                    return;
                case StateAction.Fall:

                    return;
            }
        }
        private void PlayAnimation(AnimationVariables body, AnimationVariables legs)
        {
            if(_character.ActionItem.IsTakenItem())
            {
                _stateAnimator.TryPlayAnimation(AnimationVariables.none);
            }
            else
            {
                _stateAnimator.TryPlayAnimation(body);
            }

            _stateAnimator.TryPlayAnimation(legs);
        }

        ~HandlerStateAnimation()
        {
            _character.CommonAction.ChangeStateAction -= AnimationChange;
        }
    }
}