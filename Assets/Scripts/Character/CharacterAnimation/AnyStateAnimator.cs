using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace U2D
{
    public class AnyStateAnimator
    {
        [SerializeField] private Animator _animator;
        private Player _character;
        private Dictionary<AnimationVariables, AnyStateAnimation> _animations = new Dictionary<AnimationVariables, AnyStateAnimation>();
        private AnimationVariables _currentAnimationLegs;
        private AnimationVariables _currentAnimationBody;
        private HandlerStateAnimation _handlerStateAnimation;

        public AnimationVariables CurrentAnimationLegs => _currentAnimationLegs;
        public AnimationVariables CurrentAnimationBody => _currentAnimationBody;
        public AnyStateAnimator(Player character)
        {
            CreateStatesAnimations();
            _animator = character.GetComponentInChildren<Animator>();
            _character = character;
            _handlerStateAnimation = new(this, _character);
        }
        /* public void Update()
        {
            Animate();
        } */
        public void AddAnimations(params AnyStateAnimation[] newAnimations)
        {
            for (int i = 0; i < newAnimations.Length; i++)
            {
                _animations.Add(newAnimations[i].AnimationSwitch, newAnimations[i]);
            }
        }
        public void TryPlayAnimation(AnimationVariables newAnimation)
        {
            switch (_animations[newAnimation].AnimationRig)
            {
                case RIG.BODY:
                    PlayAnimation(ref _currentAnimationBody);
                    break;
                case RIG.LEGS:
                    PlayAnimation(ref _currentAnimationLegs);
                    break;
            }

            void PlayAnimation(ref AnimationVariables currentAnimation)
            {
                if(currentAnimation != newAnimation && 
                    !_animations[newAnimation].HigherPrio.Contains(currentAnimation) && 
                    _animations[currentAnimation].AnimationPriority <= _animations[newAnimation].AnimationPriority || 
                    !_animations[currentAnimation].Active)
                {
                    SetAnimationActive(currentAnimation, false);
                    SetAnimationActive(newAnimation, true);
                    currentAnimation = newAnimation;
                }
            }
        }

        private void SetAnimationActive(AnimationVariables animation, bool active)
        {
            _animations[animation].Active = active;
            _animator.SetBool(_animations[animation].Name, active);
        }
        private void Animate()
        {
            foreach (AnimationVariables item in _animations.Keys)
            {
                _animator.SetBool(_animations[item].Name, _animations[item].Active); 
            }
        }

        private void CreateStatesAnimations()
        {
            AnyStateAnimation[] anyStateAnimations = new AnyStateAnimation[]
            {
                new AnyStateAnimation(RIG.BODY, "none", AnimationVariables.none, 0),
                new AnyStateAnimation(RIG.BODY, "body_idle", AnimationVariables.body_idle, 0, AnimationVariables.body_throw_item_begin, AnimationVariables.body_throw_item_end),
                new AnyStateAnimation(RIG.BODY, "body_walk_forward", AnimationVariables.body_walk_forward, 0),
                new AnyStateAnimation(RIG.BODY, "body_walk_back", AnimationVariables.body_walk_back, 0),
                new AnyStateAnimation(RIG.BODY, "body_jump_forward", AnimationVariables.body_jump_forward, 0),
                new AnyStateAnimation(RIG.BODY, "body_jump", AnimationVariables.body_jump, 0),
                new AnyStateAnimation(RIG.BODY, "body_run_forward", AnimationVariables.body_slow_run_forward, 0),
                new AnyStateAnimation(RIG.BODY, "body_run_back", AnimationVariables.body_slow_run_back, 0),
                new AnyStateAnimation(RIG.BODY, "body_climbing", AnimationVariables.body_climbing, 0),
                new AnyStateAnimation(RIG.BODY, "body_climbing_middle", AnimationVariables.body_climbing_middle, 0),
                new AnyStateAnimation(RIG.BODY, "body_throw_item_begin", AnimationVariables.body_throw_item_begin, 5),
                new AnyStateAnimation(RIG.BODY, "body_throw_item_end", AnimationVariables.body_throw_item_end, 5),
                new AnyStateAnimation(RIG.BODY, "body_reload_rifle", AnimationVariables.body_reload_rifle, 5),

                new AnyStateAnimation(RIG.LEGS, "legs_idle", AnimationVariables.legs_idle, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_walk_forward", AnimationVariables.legs_walk_forward, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_walk_back", AnimationVariables.legs_walk_back, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_jump_forward", AnimationVariables.legs_jump_forward, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_jump", AnimationVariables.legs_jump, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_run_forward", AnimationVariables.legs_slow_run_forward, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_run_back", AnimationVariables.legs_slow_run_back, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_climbing", AnimationVariables.legs_climbing, 0),
                new AnyStateAnimation(RIG.LEGS, "legs_climbing_middle", AnimationVariables.legs_climbing_middle, 0)
            };
            AddAnimations(anyStateAnimations);
        }
    }

    public enum AnimationVariables 
    {
        none,
        body_idle,
        body_walk_forward,
        body_walk_back,
        body_jump_forward,
        body_jump,
        body_slow_run_forward,
        body_slow_run_back,
        body_climbing,
        body_climbing_middle,
        body_throw_item_begin,
        body_throw_item_end,
        body_reload_rifle,

        legs_idle,
        legs_walk_forward,
        legs_walk_back,
        legs_jump_forward,
        legs_jump,
        legs_slow_run_forward,
        legs_slow_run_back,
        legs_climbing,
        legs_climbing_middle
    }
}