using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public enum RIG {BODY, LEGS};
    public class AnyStateAnimation
    {
        public bool Active { get; set; }
        public int AnimationPriority {get; private set;}
        public string Name{get; private set;}
        public RIG AnimationRig {get; private set;}
        public AnimationVariables[] HigherPrio{get; private set;}
        public AnimationVariables AnimationSwitch{get; private set;}
        public AnyStateAnimation(RIG rig, string name, AnimationVariables animationSwitch, int priority, params AnimationVariables[] higherPrio)
        {
            AnimationRig = rig;
            Name = name;
            HigherPrio = higherPrio;
            AnimationSwitch = animationSwitch;
            AnimationPriority = priority;
        }
        
    }
}
