using System;
using UnityEngine;
using UnityEngine.Events;

namespace U2D
{
    //Общий класс для действий. Должен объявлять все действия. Имеет общие поля, используемые одновременно всеми классами
    public class CharacterCommonAction
    {
        public event UnityAction<StateAction> ChangeStateAction;

        private Player _character;
        private CharacterPhysics _actionCharacterPhysics;
        private CharacterMove _actionCharacterMove;
        private CharacterJump _actionCharacterJump;
    
        public bool Grounded {get; set;}
        public float Friction {get; set;}
        public Vector2 Velocity {get; set;}
        public Player Character => _character;
        public CharacterPhysics ActionCharacterPhysics => _actionCharacterPhysics;
        public CharacterMove ActionCharacterMove => _actionCharacterMove;
        public CharacterJump ActionCharacterJump => _actionCharacterJump;
        public CharacterRotation ActionCharacterRotation {get; private set;}
        public AudioSource Audio {get; private set;}

        public CharacterCommonAction(Player character)
        {
            _character = character;
            _actionCharacterPhysics = new(this, _character.PhysicsProperty);
            _actionCharacterMove = new(this, _character.MoveProperty);
            _actionCharacterJump = new(this, _character.JumpProperty);
            _character.ActionsFixedUpdate.Add(_actionCharacterMove);
            _character.ActionsFixedUpdate.Add(_actionCharacterPhysics); 
            _character.ActionsFixedUpdate.Add(_character.ActionItem);

            ActionCharacterRotation = new(character);
            _character.ActionsLateUpdate.Add(ActionCharacterRotation);

            Audio = _character.GetComponent<AudioSource>();
        }

        public void ApplyStateAction(StateAction state)
        {
            ChangeStateAction?.Invoke(state);
        }
    }

    public enum StateAction{None, Idle, RunForward, RunBack, Jump, Fall}
}