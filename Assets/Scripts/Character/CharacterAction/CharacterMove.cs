using UnityEngine;

namespace U2D
{
    public class CharacterMove : IActionFixedUpdate
    {
        public enum Direction {left = -1, right = 1}
        private bool _move;
        private CharacterMoveProperty _property;
        private CharacterCommonAction _commonActon;

        public CharacterMove(CharacterCommonAction commonActon, CharacterMoveProperty property)
        {
            _commonActon = commonActon;
            _property = property;
        }

        public void MoveLeft()
        {
            if(_commonActon.Grounded)
            {
                SetVelocity(Direction.left, _property.AccelerationGround);
                if(_commonActon.ActionCharacterRotation.FlipBody < 0)
                    _commonActon.ApplyStateAction(StateAction.RunForward);
                else
                    _commonActon.ApplyStateAction(StateAction.RunBack);
            }
            else
            {
                SetVelocity(Direction.left, _property.AccelerationAir);
            }
        }
        public void MoveRight()
        {
            if(_commonActon.Grounded)
            {
                SetVelocity(Direction.right, _property.AccelerationGround);
                if(_commonActon.ActionCharacterRotation.FlipBody > 0)
                    _commonActon.ApplyStateAction(StateAction.RunForward);
                else
                    _commonActon.ApplyStateAction(StateAction.RunBack);
            }
            else
            {
                SetVelocity(Direction.right, _property.AccelerationAir);
            }
        }

        public void CustomFixedUpdate()
        {   
            if((!_move || Mathf.Abs(_commonActon.Velocity.x) > _property.MaxSpeed) && _commonActon.Grounded)
            {   
                if (Mathf.Abs(_commonActon.Velocity.x) > _commonActon.Friction * Time.fixedDeltaTime)
                    _commonActon.Velocity = new Vector2(_commonActon.Velocity.x / _property.FrictionScale * _commonActon.Friction, _commonActon.Velocity.y);
                else 
                    _commonActon.Velocity = new Vector2(0, _commonActon.Velocity.y);
            }
            _move = false;
        }

        private void SetVelocity(Direction direction, float acceleration)
        {
            _move = true;
            ///Debug.Log($"(VelocityInputBXX: {_commonActon.Velocity.x}, VelocityInputBXY: {_commonActon.Velocity.y})");
           if(Mathf.Abs(_commonActon.Velocity.x) < _property.MaxSpeed || Mathf.Sign(_commonActon.Velocity.x) != (float)direction)
           {
                _commonActon.Velocity += new Vector2(Time.deltaTime * acceleration * (float)direction, 0);
           }
            ///Debug.Log($"(VelocityInputAXX: {_commonActon.Velocity.x}, VelocityInputAXY: {_commonActon.Velocity.y})");
            ///Debug.Log("---------------------------------------------------------------------");
        }
    }
}