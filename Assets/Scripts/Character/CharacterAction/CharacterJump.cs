using UnityEngine;

namespace U2D
{
    public class CharacterJump
    {
        private CharacterJumpProperty _property;
        private CharacterCommonAction _commonActon;

        public CharacterJump(CharacterCommonAction commonAction, CharacterJumpProperty property)
        {
            _commonActon = commonAction;
            _property = property;
        }

        public void Jump()
        { 
            if(_commonActon.Grounded)
            {
                _commonActon.Velocity += Vector2.up * _property.ForceJump;
                _commonActon.ApplyStateAction(StateAction.Jump);
            }
        }
    }
}