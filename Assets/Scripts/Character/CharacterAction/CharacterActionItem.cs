using System.Collections;
using UnityEngine;

namespace U2D
{
    public class CharacterActionItem : IActionFixedUpdate
    {
        public const float  WaitUntilPutAway = 2f;
        private bool _isTaken;
        protected bool _reload;
        private int _currentQuickSlot;
        protected Player _character;
        protected InventoryItem _currentInventoryItem;
        protected Coroutine _waitPutAway;

        private bool _isItemAnimated;
        public Inventory ItemInventory {get; private set;} 
        public QuickInventory Quick {get; private set;} 

        public CharacterActionItem(Player character)
        {
            _character = character;
            ItemInventory = new();
            Quick = new(this);
        }

        public void Action()
        {
            if(IsTaken(true))
                _currentInventoryItem.GetItem().Action(GetKeyState.GetKey);
        }
         public void ActionUp()
        {
            if(IsTaken(true))
                _currentInventoryItem.GetItem().Action(GetKeyState.GetKeyUp);
        }
         public void ActionDown()
        {
            if(IsTaken(true))
                _currentInventoryItem.GetItem().Action(GetKeyState.GetKeyDown);
        }

        //Подобрать предмет
        public virtual void PickUp(ICommonItem item)
        {
            item.PickUp(_character);
            ItemInventory.Add(item, item.Amount);            
        }

        //Выбрать предмет
        //надо переделать
        public void QuickEquip(int value)//екипировать из быстрого слота
        {
            if(Quick.GetInventoryItem(value) == null) return;

            _currentQuickSlot = value;
            Quick.GetInventoryItem(value).Equip(true);
        }

        public virtual bool Equip(InventoryItem item, bool isEquip)
        {
            if(item == _currentInventoryItem && isEquip) return true;//если предметы совпадают вернуть истина, значит экипировать ничего не нужно

            if(_currentInventoryItem != null && isEquip)//если не совпадает , значит мы хотим экипировать другой предмет, поэтому текущий разэкипируем
            {
                _currentInventoryItem.Equip(false);
            }
            
            if(isEquip)
            {
                _currentInventoryItem = item;
                item.GetItem().ItemCommonAction.CommonAnimator.OnAnimationEnd += EndAnimation;
            }
            else
            {
                _currentInventoryItem = null;
                item.GetItem().ItemCommonAction.CommonAnimator.OnAnimationEnd -= EndAnimation;
            }
            return false;
        }

        public void TryTake()
        {
            Take(_isTaken);
        }

        public bool IsTaken(bool isTaken)
        {
            if(_isItemAnimated) return false;
            if(_isTaken == isTaken) return true;//если предмет уже взят или если уже не взят, выходим
            Take(isTaken);//иначе берем
            return false;
        }

        public void Take(bool isTaken)
        {
            if(_currentInventoryItem == null) return;//Проверяем можно ли взять в руки предмет
            
            _isItemAnimated = true;
            _currentInventoryItem.GetItem().Take(isTaken);
            _currentInventoryItem.GetItem().ItemCommonAction.Arming(isTaken);
            _isTaken = isTaken;
        }

        public void EndAnimation()
        {
             _isItemAnimated = false;
             _currentInventoryItem.GetItem().IsTaken = _isTaken;
             if(_isTaken)
            {
                _character.Rig.RightLimbSolver2D.weight = 1;
                _character.Rig.LeftLimbSolver2D.weight = 1;
            }
            else
            {
                _character.Rig.RightLimbSolver2D.weight = 0;
                _character.Rig.LeftLimbSolver2D.weight = 0;
            }
        }

        public void Reload()//Вызов из оружия
        {
            if(IsTaken(true))//Если в руках нет оружия, но оно экипировно, то берем в руки, после этого делаем перезарядку
            {
                int remain = 0;
                Weapon weapon = _currentInventoryItem.GetItem() as Weapon;
                if(weapon != null)
                {
                    InventoryItem item = ItemInventory.GetInventoryItem(GetAmmoType(weapon));
                    if (item != null) 
                    {
                        ItemInventory.Remove(item.GetItem(), weapon.Config.ClipCapacity - weapon.ItemCommonAction.CurrentClipCapacity, out remain);
                    }
                    weapon.Reload(remain);
                }
            }
        }
        public string GetAmmoType(ICommonItem commonItem)
        {
            return commonItem == null ? "" : commonItem.ItemCommonAction.AmmoTypeConfigs[commonItem.ItemCommonAction.CurrenAmmoType].ItemType;
        }

        public void TryPutAway()
        {
            _waitPutAway = _character.StartCoroutine(PutAway());
        }

        private IEnumerator PutAway()
        {
            _reload = true;
            yield return new WaitForSeconds(WaitUntilPutAway);
            if(_reload)
            {
                IsTaken(false);
                _reload = false;
            }
        }

        public void TakeDamag(float damage)
        {
            
        }

        public void CustomFixedUpdate()
        {
            RaycastHit2D[] hit2D = new RaycastHit2D[1];
            _character.CommonAction.ActionCharacterPhysics.CommoRB2D.Cast(Vector2.zero, _character.PhysicsProperty.ItemCollisionFilter2D, hit2D);

            if(hit2D[0])
            {
                ICommonItem item = hit2D[0].transform.GetComponent<ICommonItem>();
                if (item != null)
                {
                    PickUp(item);
                }
            }
        }

        public bool IsTakenItem()
        {
            return _isTaken && _currentInventoryItem != null;
        }
    }
}