using System;
using UnityEngine;

namespace U2D
{
    public class PlayerActionItem : CharacterActionItem
    {
        public Action<Sprite, Sprite> OnChangedUIIcon;
        public Action<HUDTextType, int> OnChangedHUDText;
        public PlayerActionItem(Player character) : base(character)
        {
        }

        public void ClipStateChangedHUD(int amount)
        {
            OnChangedHUDText(HUDTextType.Clip, amount);
        }
        public void AmmoStateChangedHUD(string config)
        {
            InventoryItem item = ItemInventory.GetInventoryItem(config);
            int amount = 0;
            if(item != null) amount = item.GetAmount();
            OnChangedHUDText(HUDTextType.Ammo, amount);
        }
        public void IconStateChangedHUD()
        {
            if(_currentInventoryItem == null)
            {
                OnChangedUIIcon(null, null);
            }
            else
            {
                OnChangedUIIcon(_currentInventoryItem.GetItem().ItemConfig.IconInInventory, _currentInventoryItem.GetItem().ItemConfig.AmmoTypeConfig[_currentInventoryItem.GetItem().ItemCommonAction.CurrenAmmoType].IconInInventory);
            }
        }

        public override bool Equip(InventoryItem item, bool isEquip)
        {
            bool isEquiped =  base.Equip(item, isEquip);
            if(isEquip)
            {
                ClipStateChangedHUD(item.GetItem().ItemCommonAction.CurrentClipCapacity);
                AmmoStateChangedHUD(GetAmmoType(item.GetItem()));
                IconStateChangedHUD();
            }
            return isEquiped;
        }

        public override void PickUp(ICommonItem item)
        {
            base.PickUp(item);
            AmmoStateChangedHUD(GetAmmoType(item));
        }

        public void ReloadByInput()//Вызов по нажатию клавиши
        {
            if(!_reload) return;
            _reload = false;
            _character.StopCoroutine(_waitPutAway);
            Reload();
        }

        public void UpdateHUD()
        {
            if(_currentInventoryItem == null)
            {
                ClipStateChangedHUD(0);
                AmmoStateChangedHUD("");
                IconStateChangedHUD();
            }
            else
            {
                ClipStateChangedHUD(_currentInventoryItem.GetItem().ItemCommonAction.CurrentClipCapacity);
                AmmoStateChangedHUD(GetAmmoType(_currentInventoryItem.GetItem()));
            }
        }

        public void ShowInventory()
        {
            _character.World.GlobalGameState.Value = GameState.InventoryMenu;
        }
    }
}