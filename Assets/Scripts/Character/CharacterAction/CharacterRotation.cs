using UnityEngine;
using UnityEngine.U2D.IK;
using UnityEngine.UIElements;

namespace U2D
{
    public class CharacterRotation : IActionLateUpdate
    {
        private Player _character;
        public float FlipBody {get; private set;} = 1;
        public Vector3 RotationBody {get; private set;}

        public CharacterRotation(Player character)
        {
            _character = character;
        }

        public void CustomLateUpdate()
        {
            BodyRotation(_character, CameraTracker.DirectionToMouse.normalized, FlipBody);
            Flip();
            _character.transform.localScale = new(FlipBody  * Mathf.Abs(_character.transform.localScale.x), _character.transform.localScale.y);
        }

        private void Flip()
        {
            FlipBody = Mathf.Sign(CameraTracker.DirectionToMouse.x);
        }

        public static void BodyRotation(Player character, Vector3 direction, float flip)
        {
            direction = new Vector3(direction.x * flip, direction.y, 0);
            if(CameraTracker.DirectionToMouse.y >= 0)
            {
                character.Rig.BoneSpine1.localRotation = GetRotate(character.Rig.WeightRotationUpSpine1);
                character.Rig.BoneSpine2.localRotation = GetRotate(character.Rig.WeightRotationUpSpine2);
                character.Rig.BoneHead.localRotation = GetRotate(character.Rig.WeightRotationUpHead);
            }
            else
            {
                character.Rig.BoneSpine1.localRotation = GetRotate(character.Rig.WeightRotationDownSpine1);
                character.Rig.BoneSpine2.localRotation = GetRotate(character.Rig.WeightRotationDownSpine2);
                character.Rig.BoneHead.localRotation = GetRotate(character.Rig.WeightRotationDownHead);
            }

            Quaternion GetRotate(float weight)
            {
                return Quaternion.Lerp(Quaternion.FromToRotation(Vector3.right, direction), Quaternion.identity,  1 - weight);
            }
        }

    }
}