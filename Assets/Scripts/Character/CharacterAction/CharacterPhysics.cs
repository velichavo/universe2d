using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    public class CharacterPhysics : IActionFixedUpdate
    {
        private bool _collision, _jumpOff;
        private Vector2 _groundNormal = Vector2.up;
        private Vector2 newMovementX, newMovementY; 
        private readonly RaycastHit2D[] _hitBuffer = new RaycastHit2D[16];
        private readonly List<RaycastHit2D> _hitBufferList = new(16);
        private readonly CharacterPhysicsProperty _property;
        private CharacterCommonAction _commonActon;

        public Rigidbody2D CommoRB2D;

        public CharacterPhysics(CharacterCommonAction commonAction, CharacterPhysicsProperty property)
        {
            _commonActon = commonAction;
            _property = property;
            //property.Reset();
            commonAction.Velocity = Vector2.up;
            CommoRB2D = _commonActon.Character.GetComponent<Rigidbody2D>();
        }

        public void CustomFixedUpdate()
        {
            Move();
        }
        private void Move()
        {
            _commonActon.Velocity += _property.GravityScale * Physics2D.gravity * Time.fixedDeltaTime;

            _collision = false;
            _commonActon.Grounded = false;
            _commonActon.Friction = 0;
            Vector2 deltaPosition = _commonActon.Velocity * Time.fixedDeltaTime;
            
            //x
            Vector2 moveAlongGround = new (_groundNormal.y, -_groundNormal.x);
            Vector2 move = moveAlongGround * deltaPosition.x;
            newMovementX = GetMovement(move, false);
            CommoRB2D.position += newMovementX;
            //y
            move = Vector2.up * deltaPosition;
            newMovementY = GetMovement(move, true);
            CommoRB2D.position += newMovementY;

            if (_commonActon.Velocity.y < 0 && !_commonActon.Grounded)
            {
                _commonActon.ApplyStateAction(StateAction.Fall);
            } 
        }

        private Vector2 GetMovement(Vector2 move, bool yMovement)
        {
            float distance = move.magnitude;
            Vector2 newGroundNormal = Vector2.zero;
            Vector2 pushVector = Vector2.zero;
            if (distance > _property.MinMoveDistance) 
            {   
                int count = CommoRB2D.Cast(move, _property.CollisionFilter2D, _hitBuffer, distance  + _property.ShellRadius);
                _hitBufferList.Clear ();

                for (int i = 0; i < count; i++) 
                {
                    _hitBufferList.Add (_hitBuffer [i]);
                }

                for (int i = 0; i < _hitBufferList.Count; i++) 
                {
                    Vector2 currentNormal = _hitBufferList[i].normal;
                    _collision = true;
                    if (currentNormal.y > _property.SlopeHeight)
                    {      
                        _commonActon.Grounded = true;
                        if (yMovement)
                        {
                            newGroundNormal = (currentNormal + newGroundNormal).normalized;
                            _groundNormal = newGroundNormal;
                            currentNormal.x = 0;
                            if(_commonActon.Velocity.x == 0)
                            {
                                _commonActon.ApplyStateAction(StateAction.Idle);
                            }
                        }
                    }
                    else
                    {
                        //считается, только когда персонаж не на поверхности. Убрал под условие, потому что персонаж съезжал с наклонных поверхностей больше SlopeHeight
                        pushVector = currentNormal + pushVector;
                    }
                    float modifiedDistance = _hitBufferList[i].distance - _property.ShellRadius;
                    if(modifiedDistance < distance)
                    {
                        distance = modifiedDistance;
                        //берем трение из свойства колайдера из минимальной дистанции до всех колайдеров
                        _commonActon.Friction = _hitBufferList[i].collider.friction;
                    }
                   
                    float projection = Vector2.Dot(_commonActon.Velocity, currentNormal);
                    if (projection < 0 || _commonActon.Grounded)
                    {
                        _commonActon.Velocity -= projection * currentNormal;
                    }
                }
                if(pushVector.x + pushVector.y < 0)
                {
                    move = pushVector.normalized;
                }
                move = move.normalized * distance;
            }  
            return move;           
        }
        public void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(CommoRB2D.position + Vector2.down, CommoRB2D.position + Vector2.down + (newMovementX + newMovementY).normalized);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(CommoRB2D.position, CommoRB2D.position + _groundNormal);

        }
    }
}