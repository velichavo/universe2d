namespace U2D
{
    public interface IActionLateUpdate
    {
        public void CustomLateUpdate();
    }
}