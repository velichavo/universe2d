using System.Diagnostics;

namespace U2D
{
    public class SwitchQuickSlot
    {
        private CharacterActionItem _actionItem;

        public SwitchQuickSlot(CharacterActionItem actionItem)
        {
            _actionItem = actionItem;
        }
        public void SwitchQuickSlotAlpha1()
        {
            _actionItem.QuickEquip(0);
        }
        public void SwitchQuickSlotAlpha2()
        {
            _actionItem.QuickEquip(1);
        }
        public void SwitchQuickSlotAlpha3()
        {
            _actionItem.QuickEquip(2);
        }
        public void SwitchQuickSlotAlpha4()
        {
            _actionItem.QuickEquip(3);
        }
        public void SwitchQuickSlotAlpha5()
        {
            _actionItem.QuickEquip(4);
        }
        public void SwitchQuickSlotAlpha6()
        {
            _actionItem.QuickEquip(5);
        }
        public void SwitchQuickSlotAlpha7()
        {
            _actionItem.QuickEquip(6);
        }
        public void SwitchQuickSlotAlpha8()
        {
            _actionItem.QuickEquip(7);
        }
        public void SwitchQuickSlotAlpha9()
        {
            _actionItem.QuickEquip(8);
        }
        public void SwitchQuickSlotAlpha0()
        {
            _actionItem.QuickEquip(9);
        }
        public void SwitchQuickSlotAlphaLeft()
        {
            _actionItem.QuickEquip(10);
        }
        public void SwitchQuickSlotAlphaRight()
        {
            _actionItem.QuickEquip(11);
        }
    }
}