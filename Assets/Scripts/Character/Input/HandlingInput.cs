using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class HandlingInput : IActionUpdate
    {
        private List<Command> _commands;

        public void Init(List<Command> commands)
        {
            _commands = commands;
        }
        public void HandleInput()
        {
            
            foreach (Command command in _commands)
            {
                if(command.AxisName != InputAxisName.None)
                {
                    command.GetAxisRaw(Input.GetAxisRaw(command.AxisName.ToString()));
                }
                if(Input.GetKeyDown(command.Key))
                {
                    command.GetKeyDown();
                }
                if(Input.GetKeyUp(command.Key))
                {
                    command.GetKeyUp();
                }
                if(Input.GetKey(command.Key))
                {
                    command.GetKey();
                }
            }
        }

        public void CustomUpdate()
        {
            HandleInput();
        }
    }
}
