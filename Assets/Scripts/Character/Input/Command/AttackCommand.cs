using System;
using UnityEngine;

namespace U2D
{
    public class AttackCommand : Command
    {
        public AttackCommand(Action action, KeyCode key, bool activeInInterface = false) : base(action, key)
        {
        }
        public override void GetKey()
        {
            _actionHandler.Invoke();
        }
    }
}