using System;
using UnityEngine;

namespace U2D
{
        public class AttackDownCommand : Command
    {
        public AttackDownCommand(Action action, KeyCode key, bool activeInInterface = false) : base(action, key)
        {
        }
        public override void GetKeyDown()
        {
            _actionHandler.Invoke();
        }
    }
}