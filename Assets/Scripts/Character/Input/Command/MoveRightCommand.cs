using System;
using UnityEngine;

namespace U2D
{
    public class MoveRightCommand : Command
    {
        public MoveRightCommand(Action action, InputAxisName axisName, bool activeInInterface = false) : base(action, axisName)
        {
        }
        public override void GetAxisRaw(float value)
        {
            if(value > 0)
            {
                _actionHandler.Invoke();
            }
        }
    }
}

