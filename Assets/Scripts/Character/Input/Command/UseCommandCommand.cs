using System;
using UnityEngine;

namespace U2D
{
    public class UseCommand : Command
    {
        public UseCommand(Action action, KeyCode key, bool activeInInterface = false) : base(action, key)
        {
        }
        public override void GetKeyDown()
        {
            //character.Actions.BeginJump();
        }
    }
}

