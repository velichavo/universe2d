
using System;
using UnityEngine;

namespace U2D
{
    public class EquipCommand : Command
    {
        public EquipCommand(Action action, KeyCode key, bool activeInInterface = false) : base(action, key)
        {

        }
        public override void GetKeyDown()
        {
            _actionHandler.Invoke();
        }
    }
}
