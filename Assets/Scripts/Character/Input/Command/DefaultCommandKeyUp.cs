using System;
using UnityEngine;

namespace U2D
{
    public class DefaultCommandKeyUp : Command
    {
        public DefaultCommandKeyUp(Action action, KeyCode key, bool activeInInterface = false) : base(action, key)
        {
        }
        public override void GetKeyUp()
        {
            _actionHandler.Invoke();
        }
    }
}
