using System;
using UnityEngine;

namespace U2D
{
    public class MoveLeftCommand : Command
    {
        public MoveLeftCommand(Action action, InputAxisName axisName, bool activeInInterface = false) : base(action, axisName)
        {
        }
        public override void GetAxisRaw(float value)
        {
            if(value < 0)
            {
                _actionHandler.Invoke();
            }
        }
    }
}

