using System;
using UnityEngine;

namespace U2D{
    public enum GetKeyState {GetKey, GetKeyDown, GetKeyUp}
    public enum InputAxisName {None, Horizontal, Vertical}
    public enum InputActionCommand {None, MoveRight, MoveLeft, Jump, Action, EquipAlpha1, EquipAlpha2, EquipAlpha3, EquipAlpha4, EquipAlpha5, EquipAlpha6, EquipAlpha7, EquipAlpha8, EquipAlpha9, EquipAlpha0, EquipLeft, EquipRight,
                                    ActionUp, ActionDown, ShowInventory}
    public abstract class Command
    {
        protected Action _actionHandler;
        public InputAxisName AxisName {get; private set;}
        public KeyCode Key {get; private set;}
        public Command(Action action, KeyCode key = KeyCode.None)
        {
            _actionHandler = action;
            Key = key;
            AxisName = InputAxisName.None;
        }
        public Command(Action action, InputAxisName axisName = InputAxisName.None)
        {
            _actionHandler = action;
            Key = KeyCode.None;
            AxisName = axisName;;
        }
        public virtual void GetKeyDown()
        {

        }
        public virtual void GetKeyUp()
        {
            
        }
        public virtual void GetKey()
        {
            
        }
        public virtual void GetAxisRaw(float value)
        {

        }
    }
}
