using System;
using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    [RequireComponent(typeof(Rigidbody2D), typeof(CapsuleCollider2D))]
    public class Player : Entity
    {
        [SerializeField] private DefaultCharacterConfig _playerConfig;
        //property
        [SerializeField] private CharacterParametersProperty _characterProperty;
        [SerializeField] private CharacterMoveProperty _characterMoveProperty;
        [SerializeField] private CharacterPhysicsProperty _characterPhysicsProperty;
        [SerializeField] private CharacterJumpProperty _characterJumpProperty;
        [SerializeField] private PlayerInputKey _inputKey;
        [SerializeField] private CharacterRigPass _characterRigPass;
        //action

        private HandlingInput _handlingInput;
        private CharacterCommonAction _characterCommonAction;
        private AnyStateAnimator _anyStateAnimator;

        public CharacterRigPass Rig => _characterRigPass;
        public CharacterCommonAction CommonAction => _characterCommonAction;
        public CharacterParametersProperty ParametersProperty => _characterProperty;
        public CharacterMoveProperty MoveProperty => _characterMoveProperty;
        public CharacterPhysicsProperty PhysicsProperty => _characterPhysicsProperty;
        public CharacterJumpProperty JumpProperty => _characterJumpProperty;
        public PlayerActionItem ActionItem {get; private set;}
        public GameObject GameObject { get {return gameObject;} }

        public override void Init()
        {
            ActionItem = new(this);
            PropertyInit();
            _characterCommonAction = new(this);
            InputInit();
            _anyStateAnimator = new(this);
            //_characterActionsUpdate.Add(_anyStateAnimator);
            
            _characterRigPass.Init(this);
        }

        private void PropertyInit()
        {
            _characterProperty = new(_playerConfig);
            _characterMoveProperty = new(_playerConfig);
            _characterPhysicsProperty = new(_playerConfig);
            _characterJumpProperty = new(_playerConfig);
        }

        private void InputInit()
        {
            _inputKey.Init(this);
            _handlingInput = new();
            _handlingInput.Init(_inputKey.Commands);
            ActionsUpdate.Add(_handlingInput);
        }

        protected override void OnEnabled()
        {
        
        }
        protected override void OnDisabled()
        {
            
        }
    }
}