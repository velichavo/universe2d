using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "CharacterConfig", menuName = "Gameplay/Character/CharacterConfig")]
    public class DefaultCharacterConfig : ScriptableObject
    {
        [Header("Character Parameters Property")]
        [SerializeField] private float _maxHitPoint;
        [Header("Move Property")]
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _accelerationGround;
        [SerializeField] private float _accelerationAir;
        [SerializeField] private float _accelerationLiquid;
        [SerializeField] private float _frictionScale;
        [Header("Jump Property")]
        [SerializeField] private float _forceJump;
        [Header("Physics Property")]
        [SerializeField] private Vector2 _gravityScale;
        [SerializeField] private float _minMoveDistance;
        [SerializeField] private float _shellRadius;
        [SerializeField, Range(0, 1)] private float _slopeHeight;
        [SerializeField] SurfaceTypeMaterial _surfaceType;
        [SerializeField] private ContactFilter2D _contactFilter2D;
        [SerializeField] private ContactFilter2D _itemsContactFilter2D;

        public float MaxHitPoint => _maxHitPoint;
        public float MaxSpeed => _maxSpeed;
        public float AccelerationGroud => _accelerationGround;
        public float AccelerationAir => _accelerationAir;
        public float AccelerationLiquid => _accelerationLiquid;
        public float ForceJump => _forceJump;
        public  float FrictionScale => _frictionScale;
        public Vector2 GravityScale => _gravityScale;
        public float MinMoveDistance => _minMoveDistance;
        public float ShellRadius => _shellRadius;
        public float SlopeHeight => _slopeHeight;
        public ContactFilter2D  CollisionFilter2D => _contactFilter2D;
        public ContactFilter2D  ItemsCollisionFilter2D => _itemsContactFilter2D;
        public SurfaceTypeMaterial SurfaceType => _surfaceType;
    }
}