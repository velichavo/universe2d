using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.Mathematics;

[ExecuteAlways]
public class DrawLineBone : MonoBehaviour
{
    [SerializeField] float delta = 0.01f;
    [SerializeField] Transform[] _bones;
    public void OnDrawGizmos()
    {
        Handles.color = Color.white;
        for (int i = 0; i < _bones.Length; i++)
        {
            if (!_bones[i]) continue;
            //Debug.Log($"index: {i}, bone: {_bones[i].position}");
            Vector3 v1 = new(_bones[i].position.x, _bones[i].position.y, _bones[i].position.z);
            Quaternion rotation = _bones[i].rotation;
            //Vector2 v2 = _bones[i].position + _bones[i].up * .1f;
            Vector3 v2 = v1 + rotation * Vector3.up * .1f;
            Handles.DrawLine(v1, v2);
            Vector3 p1 = new Vector3(_bones[i].position.x - delta, _bones[i].position.y - delta, _bones[i].position.z);
            Vector3 p2 = new Vector3(_bones[i].position.x - delta, _bones[i].position.y + delta, _bones[i].position.z);
            Vector3 p3 = new Vector3(_bones[i].position.x + delta, _bones[i].position.y + delta, _bones[i].position.z);
            Vector3 p4 = new Vector3(_bones[i].position.x + delta, _bones[i].position.y - delta, _bones[i].position.z);
            Vector3 p5 = new Vector3(_bones[i].position.x - delta, _bones[i].position.y - delta, _bones[i].position.z);
            Handles.DrawPolyLine(new [] {p1, p2, p3, p4, p5});
        }
    }
}

