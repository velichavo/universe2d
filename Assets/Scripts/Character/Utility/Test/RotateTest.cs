using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class RotateTest : MonoBehaviour
{
    [SerializeField] private Vector3 direction;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.FromToRotation(Vector3.right, direction / 10);
    }
}
