using System.Collections.Generic;
using UnityEngine;

namespace U2D
{
    [System.Serializable]
    public class PlayerInputKey
    {
        [SerializeField] private InputAxisName axisHorizontal = InputAxisName.Horizontal;
        [SerializeField] private InputAxisName axisVertical = InputAxisName.Vertical;
        public List<Command> Commands {get; private set;}
        public void Init(Player character)
        {
            SwitchQuickSlot switchQuickSlot = character.ActionItem.Quick.SwitchQuickSlot;
            Commands = new()
            {
                new MoveLeftCommand(character.CommonAction.ActionCharacterMove.MoveLeft, axisHorizontal),
                new MoveRightCommand(character.CommonAction.ActionCharacterMove.MoveRight, axisHorizontal),
                new JumpCommand(character.CommonAction.ActionCharacterJump.Jump, KeyCode.Space),
                new AttackCommand(character.ActionItem.Action, KeyCode.Mouse0),
                new AttackUpCommand(character.ActionItem.ActionUp, KeyCode.Mouse0),
                new AttackDownCommand(character.ActionItem.ActionDown, KeyCode.Mouse0),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha1, KeyCode.Alpha1),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha2, KeyCode.Alpha2),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha3, KeyCode.Alpha3),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha4, KeyCode.Alpha4),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha5, KeyCode.Alpha5),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha6, KeyCode.Alpha6),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha7, KeyCode.Alpha7),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha8, KeyCode.Alpha8),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha9, KeyCode.Alpha9),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlpha0, KeyCode.Alpha0),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlphaLeft, KeyCode.LeftBracket),
                new EquipCommand(switchQuickSlot.SwitchQuickSlotAlphaRight, KeyCode.RightBracket),
                new ShowInventoryCommand(character.ActionItem.ShowInventory, KeyCode.I),
                new DefaultCommandKeyDown(character.ActionItem.TryPutAway, KeyCode.R),
                new DefaultCommandKeyUp(character.ActionItem.ReloadByInput, KeyCode.R)
            };
        }
    }
}