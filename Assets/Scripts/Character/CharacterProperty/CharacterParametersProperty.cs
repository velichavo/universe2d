using System;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class CharacterParametersProperty
    {
        [SerializeField] private float _hitPoint;
        private DefaultCharacterConfig _characterConfig;

        public  CharacterParametersProperty(DefaultCharacterConfig config)
        {
            if (config)
            {
                _characterConfig = config;
            }
            else
            {
                Debug.LogError("Нет, конфига при создании CharacterProperty");
            }
            Reset();
        }
        public void Reset()
        {
            _hitPoint = _characterConfig.MaxHitPoint;
        }
    }
}