using System;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class CharacterMoveProperty
    {
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _accelerationGround;
        [SerializeField] private float _accelerationAir;
        [SerializeField] private float _accelerationLiquid;
        [SerializeField] private float _frictionScale;
        private DefaultCharacterConfig _characterConfig;

        public float MaxSpeed => _maxSpeed;
        public  float AccelerationGround => _accelerationGround;
        public  float AccelerationAir => _accelerationAir;
        public  float AcelerationLiquid => _accelerationLiquid;
        public  float FrictionScale => _frictionScale;

        public CharacterMoveProperty(DefaultCharacterConfig config)
        {
            if (config)
            {
                _characterConfig = config;
            }
            else
            {
                Debug.LogError("Нет, конфига при создании CharacterMoveProperty");
            }
            Reset();
        }
        public void Reset()
        {
            _maxSpeed = _characterConfig.MaxSpeed;
            _accelerationGround = _characterConfig.AccelerationGroud;
            _accelerationAir = _characterConfig.AccelerationAir;
            _accelerationLiquid = _characterConfig.AccelerationLiquid;
            _frictionScale = _characterConfig.FrictionScale;
        }
    }
}