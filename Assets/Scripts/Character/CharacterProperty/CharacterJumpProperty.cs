using System;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class CharacterJumpProperty
    {
        [SerializeField] private float _forceJump;
        private DefaultCharacterConfig _characterConfig;
        public  float ForceJump => _forceJump;

        public CharacterJumpProperty(DefaultCharacterConfig config)
        {
            if (config)
            {
                _characterConfig = config;
            }
            else
            {
                Debug.LogError("Нет, конфига при создании CharacterMoveProperty");
            }
            Reset();
        }
        public void Reset()
        {
            _forceJump = _characterConfig.ForceJump;
        }
    }
}