using System;
using UnityEngine;

namespace U2D
{
    [Serializable]
    public class CharacterPhysicsProperty
    {
        [SerializeField] private Vector2 _gravityScale;
        [SerializeField] private float _minMoveDistance;
        [SerializeField] private float _shellRadius;
        [SerializeField, Range(0, 1)] private float _slopeHeight;
        [SerializeField] SurfaceTypeMaterial _surfaceType;
        private DefaultCharacterConfig _characterConfig;

        public Vector2 GravityScale => _gravityScale;
        public float MinMoveDistance => _minMoveDistance;
        public float ShellRadius => _shellRadius;
        public float SlopeHeight => _slopeHeight;
        public SurfaceTypeMaterial SurfaceType => _surfaceType;
        public ContactFilter2D  CollisionFilter2D {get; private set;}
        public ContactFilter2D  ItemCollisionFilter2D {get; private set;}

        public CharacterPhysicsProperty(DefaultCharacterConfig config, CharacterPhysicsProperty property = null)
        {
            if (config)
            {
                _characterConfig = config;
            }
            else
            {
                Debug.LogError("Нет, конфига при создании CharacterPhysicsProperty");
            }
            if(property == null)
            {
                Reset();
            }
        }
        public void Reset()
        {
            _gravityScale = _characterConfig.GravityScale;
            _minMoveDistance = _characterConfig.MinMoveDistance;
            _shellRadius = _characterConfig.ShellRadius;
            _slopeHeight = _characterConfig.SlopeHeight;
            _surfaceType = _characterConfig.SurfaceType;

            CollisionFilter2D = _characterConfig.CollisionFilter2D;
            ItemCollisionFilter2D = _characterConfig.ItemsCollisionFilter2D;
        }
    }
}