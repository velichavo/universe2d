using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.U2D.IK;

namespace U2D
{
    [System.Serializable]
    public class CharacterRigPass
    {
        [SerializeField, Range(0, 1)] float _weightRotationUpSpine1;
        [SerializeField, Range(0, 1)] float _weightRotationUpSpine2;
        [SerializeField, Range(0, 1)] float _weightRotationUpHead;
        [SerializeField, Range(0, 1)] float _weightRotationDownSpine1;
        [SerializeField, Range(0, 1)] float _weightRotationDownSpine2;
        [SerializeField, Range(0, 1)] float _weightRotationDownHead;
        [SerializeField] Transform _Body;
        [SerializeField] Transform _LeftUpLeg;
        [SerializeField] Transform _LeftLeg;
        [SerializeField] Transform _LeftFoot;
        [SerializeField] Transform _RightUpLeg;
        [SerializeField] Transform _RightLeg;
        [SerializeField] Transform _RightFoot;
        [SerializeField] Transform _Spine1;
        [SerializeField] Transform _Spine2;
        [SerializeField] Transform _LeftArm;
        [SerializeField] Transform _LeftForeArm;
        [SerializeField] Transform _LeftHand;
        [SerializeField] Transform _RightArm;
        [SerializeField] Transform _RightForeArm;
        [SerializeField] Transform _RightHand;
        [SerializeField] Transform _Head;
        [SerializeField] Solver2D _rigthLimbSolver2D;
        [SerializeField] Solver2D _leftLimbSolver2D;
        [SerializeField] Transform _backpack;

        public float WeightRotationUpSpine1 => _weightRotationUpSpine1;
        public float WeightRotationUpSpine2 => _weightRotationUpSpine2;
        public float WeightRotationUpHead => _weightRotationUpHead;
        public float WeightRotationDownSpine1 => _weightRotationDownSpine1;
        public float WeightRotationDownSpine2 => _weightRotationDownSpine2;
        public float WeightRotationDownHead => _weightRotationDownHead;
        public Transform BoneSpine1 => _Spine1;
        public Transform BoneSpine2 => _Spine2;
        public Transform BoneHead => _Head;
        public Solver2D RightLimbSolver2D => _rigthLimbSolver2D;
        public Solver2D LeftLimbSolver2D => _leftLimbSolver2D;
        public Transform Backpack => _backpack;

        public Dictionary<RigBone2D, Transform> RigBones2Ds {get; private set;}

        public void Init(Player character)
        {
            RigBones2Ds = new()
            {
                { RigBone2D.Body, _Body },
                { RigBone2D.LeftUpLeg, _LeftUpLeg },
                { RigBone2D.LeftLeg, _LeftLeg },
                { RigBone2D.LeftFoot, _LeftFoot },
                { RigBone2D.RightUpLeg, _RightUpLeg },
                { RigBone2D.RightLeg, _RightLeg },
                { RigBone2D.RightFoot, _RightFoot },
                { RigBone2D.Spine1, _Spine1 },
                { RigBone2D.Spine2, _Spine2 },
                { RigBone2D.LeftArm, _LeftArm },
                { RigBone2D.LeftForeArm, _LeftForeArm },
                { RigBone2D.LeftHand, _LeftHand },
                { RigBone2D.RightArm, _RightArm },
                { RigBone2D.RightForeArm, _RightForeArm },
                { RigBone2D.RightHand, _RightHand },
                { RigBone2D.Head, _Head }
            };
            foreach (var item in RigBones2Ds)
            {
                item.Value.GetComponent<CharacterBodyPart>().Init(character);
            }
        }
    }

    public enum RigBone2D
    {
        Body,
        LeftUpLeg,
        LeftLeg,
        LeftFoot,
        RightUpLeg,
        RightLeg,
        RightFoot,
        Spine1,
        Spine2,
        LeftArm,
        LeftForeArm,
        LeftHand,
        RightArm,
        RightForeArm,
        RightHand,
        Head
    }
}