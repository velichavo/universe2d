using UnityEngine;

namespace U2D
{
    public class CharacterBodyPart : MonoBehaviour, IEntityCollision
    {
        private Player _character;
        public SurfaceTypeMaterial SurfaceType { get => _character.PhysicsProperty.SurfaceType; }
        public GameObject CollisionGameobject { get => _character.gameObject; }

        public void Init(Player character)
        {
            _character = character;
        }
        public void OnBulletCollision(float damage)
        {
            _character.ActionItem.TakeDamag(damage);
        }
    }
}