using System;
using UnityEngine;

namespace U2D
{
    [CreateAssetMenu(fileName = "ConvertAnimationConfigNew", menuName = "Editor/ConvertAnimationConfigNew")]
    public class ConvertAnimationConfigPass : ScriptableObject
    {
        [SerializeField] private string[] _sourceTransformBone;
        [SerializeField] private string[] _targetTransformBone;

        public string[] SourceTransformBone => _sourceTransformBone;
        public string[] TargetTransformBone => _targetTransformBone;
     }
}