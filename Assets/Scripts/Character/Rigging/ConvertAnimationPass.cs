using System.Collections;
using System.Collections.Generic;
using U2D;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Animations;

public class ConvertAnimationPass : MonoBehaviour
{
    [SerializeField, Range(1, 100)] private int maxFrame;
    [SerializeField] public AnimationClip clip;
    [SerializeField] public AnimationClip targetClip;
    [SerializeField] private bool _clear;
    public bool Clear => _clear;
    public Transform[] SourceBone;
    public Transform[] targetBone;
    public ConvertAnimationConfigPass convertAnimationConfigPass;
    public Transform[] originBone;

    public Quaternion Rot => Quaternion.LookRotation(Vector3.forward, Vector3.up);
    public int MaxFrame => maxFrame;
}

