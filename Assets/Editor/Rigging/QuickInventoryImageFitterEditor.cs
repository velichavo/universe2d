using U2D;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(QuickInventoryImageFitter))]
public class QuickInventoryImageFitterEditor : Editor
{
    public override void OnInspectorGUI() 
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if(GUILayout.Button("SetImage", GUILayout.Width(Screen.width / 3 - 3f)))
        {
            var imageFitter = (QuickInventoryImageFitter)target;
            imageFitter.SetImageInSlot(imageFitter.TestSprite);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        base.OnInspectorGUI();
    }
}