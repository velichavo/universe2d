using U2D;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WeaponAnimatePosition))]
public class MakeAnimationWeaponEditor : Editor
{
    public override void OnInspectorGUI() 
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if(GUILayout.Button("AddPositionToCurves", GUILayout.Width(Screen.width / 3 - 3f)))
        {
            var weapon = (WeaponAnimatePosition)target;
            weapon.AddPositionToCurves();
        }
        
        if(GUILayout.Button("Set From Config", GUILayout.Width(Screen.width / 3 - 3f)))
        {
            var weapon = (WeaponAnimatePosition)target;
            weapon.SetAnimationCurveFromConfig();
        }
        if(GUILayout.Button("Clear Keys", GUILayout.Width(Screen.width / 3 - 3f)))
        {
            var weapon = (WeaponAnimatePosition)target;
            weapon.ClearKeys();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        base.OnInspectorGUI();
    }
}