using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Unity.Mathematics;
using System;

[CustomEditor(typeof(ConvertAnimationPass))]
public class ConvertAnimationEditorPass : Editor
{
    private EditorCurveBinding[] targetCurveBindings;
    private EditorCurveBinding[] sourceCurveBindings;
    private AnimationCurve[] targetAnimationCurves;
    private AnimationCurve[] sourceAnimationCurves;
    private string[] sourceStringCurveBindings;
    private string[] targetStringCurveBindings;
    private ConvertAnimationPass convertAnimation;
    private Dictionary<string, AnimationCurve> sourcePropertyNames;
    private Dictionary<string, AnimationCurve> targetPropertyNames;
    private AnimationClipSettings sourceClipSettings;
    public override void OnInspectorGUI() 
    {
        if(GUILayout.Button("Convert Animation"))
        {
            GetCurveBindings((ConvertAnimationPass)target);
            Debug.Log($"{sourceAnimationCurves.Length}");
            Convert();
            ApplyCurveBindings();
            Debug.Log("Done");
        }

        base.OnInspectorGUI();
    }

    private void Convert()
    {
        int lastKeyIndex = sourceAnimationCurves[0].keys.Length - 1;
        float maxTime = sourceAnimationCurves[0].keys[lastKeyIndex].time;
        int maxFrame = convertAnimation.MaxFrame;
        for (int i = 0; i < maxFrame; i++)
        {
            float time = (float)(i) / maxFrame * maxTime;
            SetRotationOriginBone(time);
            SetRotationTargetBone(time);
        }
    }
    private void GetCurveBindings(ConvertAnimationPass target)
    {
        convertAnimation = target;

        //if (!convertAnimation.clip || !convertAnimation.targetClip) return;

        sourceClipSettings = AnimationUtility.GetAnimationClipSettings(convertAnimation.clip);
        sourceCurveBindings = AnimationUtility.GetCurveBindings(convertAnimation.clip);
        sourceAnimationCurves = new AnimationCurve[sourceCurveBindings.Length];
        sourceStringCurveBindings = new string[sourceCurveBindings.Length];

        sourcePropertyNames = new();

        targetCurveBindings = AnimationUtility.GetCurveBindings(convertAnimation.targetClip);
        targetAnimationCurves = new AnimationCurve[targetCurveBindings.Length];
        targetStringCurveBindings = new string[targetCurveBindings.Length];
        targetPropertyNames = new();

        for(var i = 0; i < sourceAnimationCurves.Length; i ++)
        {
            sourceAnimationCurves[i] = AnimationUtility.GetEditorCurve(convertAnimation.clip, sourceCurveBindings[i]);
            sourceStringCurveBindings[i] = $"{sourceCurveBindings[i].propertyName}_{sourceCurveBindings[i].path}";
        }

        //create target animation Curves
        for (int i = 0; i < targetAnimationCurves.Length; i++)
        {
            targetAnimationCurves[i] = new();
            targetStringCurveBindings[i] = $"{targetCurveBindings[i].propertyName}_{targetCurveBindings[i].path}";
        }

    }
    private void ApplyCurveBindings()
    {
        for(var i = 0; i < targetAnimationCurves.Length; i ++)
        {
            AnimationUtility.SetEditorCurve(convertAnimation.targetClip, targetCurveBindings[i], targetAnimationCurves[i]);
        }
        //AnimationUtility.SetEditorCurves(convertAnimation.clip, sourceCurveBindings, sourceAnimationCurves);
    }

    void SetRotationTargetBone(float time)
    {
        for (int i = 0; i < convertAnimation.convertAnimationConfigPass.TargetTransformBone.Length; i++)
        {
            int indexCurve = Array.IndexOf(targetStringCurveBindings, convertAnimation.convertAnimationConfigPass.TargetTransformBone[i]);
            switch (targetCurveBindings[indexCurve].propertyName)
            {
                case "m_LocalPosition.x":
                    float x = convertAnimation.SourceBone[i].transform.localPosition.z;
                    float y = convertAnimation.SourceBone[i].transform.localPosition.y;
                    float z = 0;
                    convertAnimation.targetBone[i].transform.localPosition = new(x, y, z);
                    //Запись ключей в анимационную кривую. Если Clear истина, то ключи не записываются и анимационная кривая очищается
                    if (!convertAnimation.Clear)
                        SetAnimationCurve(time, indexCurve, x, y, z);
                    break;
                case "m_LocalRotation.x":
                    Vector3 up = convertAnimation.SourceBone[i].transform.up;
                    up = Vector3.ProjectOnPlane(up, Vector3.forward);
                    convertAnimation.targetBone[i].transform.rotation = Quaternion.FromToRotation(Vector3.up, up);
                    x = convertAnimation.targetBone[i].transform.localRotation.x;
                    y = convertAnimation.targetBone[i].transform.localRotation.y;
                    z = convertAnimation.targetBone[i].transform.localRotation.z;
                    float w = convertAnimation.targetBone[i].transform.localRotation.w;
                    //Запись ключей в анимационную кривую. Если Clear истина, то ключи не записываются и анимационная кривая очищается
                    if (!convertAnimation.Clear)
                        SetAnimationCurve(time, indexCurve, x, y, z, w);
                    break;
            }
        }
    }

    void SetRotationOriginBone(float time)
    {
        for(int i = 0; i < convertAnimation.convertAnimationConfigPass.SourceTransformBone.Length; i++)
        {
            int indexCurve = Array.IndexOf(sourceStringCurveBindings, convertAnimation.convertAnimationConfigPass.SourceTransformBone[i]);
            if(indexCurve >= 0)
            {
                float x = sourceAnimationCurves[indexCurve].Evaluate(time);
                float y = sourceAnimationCurves[indexCurve + 1].Evaluate(time);
                float z = sourceAnimationCurves[indexCurve + 2].Evaluate(time);
                switch(sourceCurveBindings[indexCurve].propertyName)
                {
                    case "m_LocalRotation.x":
                        float w = sourceAnimationCurves[indexCurve + 3].Evaluate(time);
                        convertAnimation.originBone[i].transform.localRotation = new(x, y, z, w);
                        Debug.Log($"Rotation: {convertAnimation.originBone[i].transform.localRotation.eulerAngles}");
                        break;
                    case "m_LocalPosition.x":
                        convertAnimation.originBone[i].transform.localPosition = new(x, y, z);
                        Debug.Log($"Position: {convertAnimation.originBone[i].transform.localPosition}");
                        break;
                }
            }
        }
    }

    private void SetAnimationCurve(float time, int targetCurveIndex, params float[] q)
    {
        for (var i = 0; i < q.Length; i++)
        {
            Keyframe keyframe = new(time, q[i]);
            targetAnimationCurves[targetCurveIndex + i].AddKey(keyframe);
        }
    }

    private void ClearAnimationCurve(int begin, int count)
    {
        for (var i = begin; i < begin + count; i++)
        {
            targetAnimationCurves[i] = new();
        }

    }

}
