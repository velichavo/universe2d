using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using T2D;
using UnityEngine.Rendering.Universal;

[CustomEditor(typeof(Terrain2D))]
public class Terrain2DEditor : Editor 
{
    const string k_ShapePath = "m_ShapePath";
    const string k_ShapePathHash = "m_ShapePathHash";
    Texture nodeTexture;
    //int indexToDelete;
    static GUIStyle handleStyle = new();
    Vector3[] worldPoints;
    void OnEnable()
    {
        nodeTexture = Resources.Load<Texture>("Handle");
        if (nodeTexture == null) nodeTexture = EditorGUIUtility.whiteTexture;
        handleStyle.alignment = TextAnchor.MiddleCenter;
        handleStyle.fixedWidth = 15;
        handleStyle.fixedHeight = 15;
    }

     public override void OnInspectorGUI() 
    {
        base.OnInspectorGUI();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if(GUILayout.Button("New Mesh", GUILayout.Width(Screen.width / 2 - 3f)))
        {
            Terrain2D polyline = target as Terrain2D;
            polyline.TerrainReset();
            UpdateTerrain(polyline);
        }
        if(GUILayout.Button("Nodes clear", GUILayout.Width(Screen.width / 2 - 3f)))
        {
            Terrain2D polyline = target as Terrain2D;
            polyline.NodesClear();
            UpdateTerrain(polyline);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }
    void OnSceneGUI()
    {
        Terrain2D polyline = (target as Terrain2D);
        Vector3[] localPoints = polyline.GetNodes();
        worldPoints = new Vector3[localPoints.Length];
        for (int i = 0; i < worldPoints.Length; i++)
            worldPoints[i] = polyline.transform.TransformPoint(localPoints[i]);
        DrawPolyLine(worldPoints);
        DrawNodes(polyline, worldPoints);
        DrawBounds(polyline);
        if(Event.current.alt)
        {
            Vector3 nodeOnPoly = HandleUtility.ClosestPointToPolyLine(worldPoints);
            float handleSize = HandleUtility.GetHandleSize(nodeOnPoly);
            int nodeIndex = FindNodeIndex(worldPoints, nodeOnPoly);
            Vector3 distance = worldPoints[nodeIndex - 1] - worldPoints[nodeIndex];
            Handles.Label(worldPoints[nodeIndex] + distance / 2, distance.magnitude.ToString());
        }
        if (Event.current.shift)
        {
            //Adding Points
            Vector3 mousePos = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
            Vector3 polyLocalMousePos = polyline.transform.InverseTransformPoint(mousePos);
            Vector3 nodeOnPoly = HandleUtility.ClosestPointToPolyLine(worldPoints);
            float handleSize = HandleUtility.GetHandleSize(nodeOnPoly);
            int nodeIndex = FindNodeIndex(worldPoints, nodeOnPoly);
            Handles.DrawLine(worldPoints[nodeIndex - 1], mousePos);
            Handles.DrawLine(worldPoints[nodeIndex], mousePos);
            if (Handles.Button(mousePos, Quaternion.identity, handleSize * 0.09f, handleSize, HandleFunc))
            {
                polyLocalMousePos.z = 0;
                Undo.RecordObject(polyline, "Insert Node");
                polyline.InsertNode(nodeIndex, polyLocalMousePos);
                ///////////////////////////////////////
                UpdateTerrain(polyline);
                ///////////////////////////////////////
                Event.current.Use();
            }
        }
        if (Event.current.control)
        {
            //Deleting Points
            int indexToDelete = FindNearestNodeToMouse(worldPoints);
            Handles.color = Color.red;
            float handleSize = HandleUtility.GetHandleSize(worldPoints[0]);
            if (Handles.Button(worldPoints[indexToDelete], Quaternion.identity, handleSize * 0.09f, handleSize, DeleteHandleFunc))
            {
                Undo.RecordObject(polyline, "Remove Node");
                polyline.RemoveNode(indexToDelete);
                ///////////////////////////////////////
                UpdateTerrain(polyline);
                ///////////////////////////////////////
                indexToDelete = -1;
                Event.current.Use();
            }
            Handles.color = Color.white;
        }

    }
    private int FindNearestNodeToMouse(Vector3[] worldNodesPositions)
    {
        Vector3 mousePos = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
        mousePos.z = 0;
        int index = -1;
        float minDistnce = float.MaxValue;
        for (int i = 0; i < worldNodesPositions.Length; i++)
        {
            float distance = Vector3.Distance(worldNodesPositions[i], mousePos);
            if (distance < minDistnce)
            {
                index = i;
                minDistnce = distance;
            }
        }
        return index;
    }
    private int FindNodeIndex(Vector3[] worldNodesPositions, Vector3 newNode)
    {
        float smallestdis = float.MaxValue;
        int prevIndex = 0;
        for (int i = 1; i < worldNodesPositions.Length; i++)
        {
            float distance = HandleUtility.DistanceToPolyLine(worldNodesPositions[i - 1], worldNodesPositions[i]);
            if (distance < smallestdis)
            {
                prevIndex = i - 1;
                smallestdis = distance;
            }
        }
        return prevIndex + 1;
    }
    private static void DrawPolyLine(Vector3[] nodes)
    {
        if (Event.current.shift) Handles.color = Color.green;
        else if (Event.current.control) Handles.color = Color.red;
        else Handles.color = Color.white;
        Handles.DrawPolyLine(nodes);
        Handles.color = Color.white;
    }

    private void DrawBounds(Terrain2D polyline)
    {
        Bounds bounds = polyline.GetComponent<MeshFilter>().sharedMesh.bounds;
        Handles.color = Color.grey;
        Vector3 position = polyline.transform.position;
        Vector3 newBoundsMin = new(bounds.min.x * polyline.transform.localScale.x, bounds.min.y * polyline.transform.localScale.y);
        Vector3 newBoundsMax = new(bounds.max.x * polyline.transform.localScale.x, bounds.max.y * polyline.transform.localScale.y);
        Handles.DrawDottedLine(newBoundsMin + position, newBoundsMax + position, 1f);
        //Handles.DrawDottedLine(bounds.min + position, new Vector3(bounds.min.x + position.x, bounds.max.y + position.y), .1f);
        //Handles.DrawDottedLine(new Vector3(bounds.min.x, bounds.max.y), bounds.max, .1f);
        //Handles.DrawDottedLine(bounds.min, new Vector3(bounds.min.y, bounds.max.x), .1f);
        //Handles.DrawDottedLine(new Vector3(bounds.min.y, bounds.max.x), bounds.max, .1f);

    }
    private void DrawNodes(Terrain2D polyline, Vector3[] worldPoints)
    {
        for (int i = 0; i < worldPoints.Length; i++)
        {
            Vector3 pos = worldPoints[i];
            float handleSize = HandleUtility.GetHandleSize(pos);
            var fmh_166_58_638366169231840931 = Quaternion.identity; Vector3 newPos = Handles.FreeMoveHandle(pos, handleSize * 0.09f, Vector3.one, HandleFunc);
            List<Vector3> alignTo;

            //if (currentControlID == GUIUtility.hotControl)
            //{
            //    if (CheckAlignment(worldPoints, handleSize * 0.1f, i, ref newPos, out alignTo))
            //    {
            //        Handles.color = Color.green;
            //        for (int j = 0; j < alignTo.Count; j++)
            //            Handles.DrawPolyLine(newPos, alignTo[j]);
            //        Handles.color = Color.white;
            //    }
            //}
            if (newPos != pos)
            {
                if (CheckAlignment(worldPoints, handleSize * 0.1f, i, ref newPos, out alignTo))
                {
                    Handles.color = Color.green;
                    for (int j = 0; j < alignTo.Count; j++)
                        Handles.DrawPolyLine(newPos, alignTo[j]);
                    Handles.color = Color.white;
                }
                Undo.RecordObject(polyline, "Move Node");
                polyline.ChangeNode(i, polyline.transform.InverseTransformPoint(newPos));
                ///////////////////////////////////////
                UpdateTerrain(polyline);
                ///////////////////////////////////////
            }
        }
    }
    protected void SetShape(Vector3[] path, ShadowCaster2D shadowCaster)
    {
        SerializedObject serializedObjectShadowCaster = new UnityEditor.SerializedObject(shadowCaster);

        serializedObjectShadowCaster.Update();

        var pointsProperty = serializedObjectShadowCaster.FindProperty(k_ShapePath);
        pointsProperty.arraySize = path.Length;
        //Debug.Log($"pointsProperty: {pointsProperty.name}, arraySize: {pointsProperty.arraySize}, serializedObjectShadowCaster: {serializedObjectShadowCaster.targetObject.name}");

        for (var i = 0; i < path.Length; ++i)
            pointsProperty.GetArrayElementAtIndex(i).vector3Value = path[i];
        
        var meshProperty = serializedObjectShadowCaster.FindProperty(k_ShapePathHash);

        if (shadowCaster != null)
        {
            //int hash = GetShapePathHash(shadowCaster.shapePath);
            meshProperty.intValue ++;
        }

        // This is untracked right now...
         serializedObjectShadowCaster.ApplyModifiedProperties();
    }

    private void UpdateTerrain(Terrain2D terrain)
    {
        Vector3[] localPoints = terrain.GetNodes();
        if (localPoints.Length < 3) return;
        List<Vector3> vertices = new List<Vector3>(localPoints);
        T2D.Triangulator triangulator = new T2D.Triangulator(vertices.ToArray());
        int[] indecies = triangulator.Triangulate();
        MeshFilter meshFilter = terrain.GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.sharedMesh;
        if (mesh == null) mesh = CreateMesh(terrain);
        mesh.triangles = null;
        mesh.vertices = vertices.ToArray();
        mesh.triangles = indecies;
        mesh.uv = Vec3ToVec2Array(vertices.ToArray());
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        Debug.Log(mesh.bounds);
        PolygonCollider2D collider = terrain.GetComponent<PolygonCollider2D>();
        //Set point to collider, if there
        if(collider)
            collider.points = Vec3ToVec2Array(vertices.ToArray());
        //Generate ShadowShape
        ShadowCaster2D shadowCaster = terrain.GetComponent<ShadowCaster2D>();
        if (shadowCaster) SetShape(localPoints, shadowCaster);
    }

    private Mesh CreateMesh(Terrain2D terrain)
    {
        Mesh mesh = terrain.TerrainReset();
        return mesh;
    }

    private Vector2[] Vec3ToVec2Array(Vector3[] data)
    {
        Vector2[] result = new Vector2[data.Length];
        for (int i = 0; i < data.Length; i++)
            result[i] = data[i];
        return result;
    }

    bool CheckAlignment(Vector3[] worldNodes, float offset, int index, ref Vector3 position, out List<Vector3> alignedTo)
    {
        //Debug.Log("Check aligmnet with index:" + index);
        //check vertical
        //check with the prev node
        bool aligned = false;
        //the node can be aligned to the prev and next node at once, we need to return more than one alginedTo Node
        alignedTo = new List<Vector3>(2);
        if (index > 0)
        {
            float dx = Mathf.Abs(worldNodes[index - 1].x - position.x);
            if (dx < offset)
            {
                position.x = worldNodes[index - 1].x;
                alignedTo.Add(worldNodes[index - 1]);
                aligned = true;
            }
        }
        //check with the next node
        if (index < worldNodes.Length - 1)
        {
            float dx = Mathf.Abs(worldNodes[index + 1].x - position.x);
            if (dx < offset)
            {
                position.x = worldNodes[index + 1].x;
                alignedTo.Add(worldNodes[index + 1]);
                aligned = true;
            }
        }
        //check horizontal
        if (index > 0)
        {
            float dy = Mathf.Abs(worldNodes[index - 1].y - position.y);
            if (dy < offset)
            {
                position.y = worldNodes[index - 1].y;
                alignedTo.Add(worldNodes[index - 1]);
                aligned = true;
            }
        }
        //check with the next node
        if (index < worldNodes.Length - 1)
        {
            float dy = Mathf.Abs(worldNodes[index + 1].y - position.y);
            if (dy < offset)
            {
                position.y = worldNodes[index + 1].y;
                alignedTo.Add(worldNodes[index + 1]);
                aligned = true;
            }
        }
        return aligned;
    }

    void HandleFunc(int controlID, Vector3 position, Quaternion rotation, float size, EventType eventType)
    {
        if (eventType == EventType.Layout)
        {
            AddControl(controlID, position);
        }
        else if (eventType == EventType.Repaint)
        {
            if (controlID == GUIUtility.hotControl)
                GUI.color = Color.red;
            else
                GUI.color = Color.green;
            Handles.Label(position, new GUIContent(nodeTexture), handleStyle);
            GUI.color = Color.white;
        }
    }

    void DeleteHandleFunc(int controlID, Vector3 position, Quaternion rotation, float size, EventType eventType)
    {
        if (eventType == EventType.Layout)
        {
            AddControl(controlID, position);
        }
        else if (eventType == EventType.Repaint)
        {
            GUI.color = Color.red;
            Handles.Label(position, new GUIContent(nodeTexture), handleStyle);
            GUI.color = Color.white;
        }
    }
    private static void AddControl(int controlID, Vector3 position)
    {
        Vector3 mousePos = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
        float distance = Vector2.Distance(position, mousePos);
        HandleUtility.AddControl(controlID, distance);
    }
    
}
